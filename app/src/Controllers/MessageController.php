<?php
namespace App\Controllers;

use App\Helpers\BridgeHelper;
use App\Helpers\ErrorHelper;
use App\Models\MessageQuery;
use App\Services\MessageService;
use OAuth2\Server;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class MessageController
 *
 * @package App\Controllers
 */
class MessageController {
    /**
     * @var MessageService
     */
    private $messageService;

    /**
     * @var Server
     */
    private $oauth2Server;

    /**
     * OAuth2Controller constructor.
     *
     * @param Container $container
     */
    function __construct(Container $container) {
        $this->messageService = $container->get('message_service');
        $this->oauth2Server = $container->get('oauth2_server');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function getLabelNames(Request $request, Response $response) {
        $bridge_request = BridgeHelper::bridgeRequest($request);
        if ($this->oauth2Server->verifyResourceRequest($bridge_request)) {
            return $this->messageService->getLabelNames();
        } else {
            return $response->withJson(new ErrorHelper('getLabelNamesUnauthorized', 'Unauthorized access'))->withStatus(403);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function queryLabelSets(Request $request, Response $response) {
        $bridge_request = BridgeHelper::bridgeRequest($request);
        if ($this->oauth2Server->verifyResourceRequest($bridge_request)) {
            $result = $this->messageService->queryLabelSets($request->getParams());
            if ($result instanceof ErrorHelper) {
                return $response->withJson($result)->withStatus(403);
            }
            return $response->withJson($result);
        } else {
            return $response->withJson(new ErrorHelper('queryLabelSetsUnauthorized', 'Unauthorized access'))->withStatus(403);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function postLabelSet(Request $request, Response $response) {
        $bridge_request = BridgeHelper::bridgeRequest($request);
        if ($this->oauth2Server->verifyResourceRequest($bridge_request)) {
            $message = MessageQuery::create()->findOneById($request->getParam('message_id'));
            if ($message) {
                $result = $this->messageService->addLabelSet($message, $request->getParsedBody());
                if ($result instanceof ErrorHelper) {
                    return $response->withJson($result)->withStatus(403);
                }
                return $response->withJson($result);
            } else {
                return $response->withJson(new ErrorHelper('postLabelSetError', 'Request error'))->withStatus(403);
            }
        } else {
            return $response->withJson(new ErrorHelper('postLabelSetUnauthorized', 'Unauthorized access'))->withStatus(403);
        }
    }
}