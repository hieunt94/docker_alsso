<?php
namespace App\Controllers;

use App\Helpers\BridgeHelper;
use App\Helpers\ErrorHelper;
use App\Services\OAuth2\EmailNotVerifiedException;
use OAuth2\HttpFoundationBridge\Response as BridgeResponse;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface as Logger;
use Slim\Container;
use Slim\Views\Twig;

/**
 * Class AuthController
 *
 * @package App\Controllers
 */
class AuthController {
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Twig
     */
    private $view;

    /**
     * @var \OAuth2\Server
     */
    private $server;

    /**
     * OAuth2Controller constructor.
     * @param Container $container
     */
    function __construct(Container $container) {
        $this->logger = $container->get('logger');
        $this->view = $container->get('view');
        $this->server = $container->get('oauth2_server');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function getAuthorize(Request $request, Response $response) {
        $bridge_response = new \OAuth2\Response();
        $parameters = $this->server->validateAuthorizeRequest(BridgeHelper::bridgeRequest($request), $bridge_response);
        if (!$parameters) {
            return $response->withRedirect('/error');
        }
        return $this->view->render($response, 'angular_view.twig', [
            'client_id' => $request->getParam('client_id')
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function postAuthorize(Request $request, Response $response) {
        $is_authorized = ($request->getParam('authorized') === 'Yes');
        $bridge_response = new BridgeResponse();
        // TODO change the testuser
        $this->server->handleAuthorizeRequest(BridgeHelper::bridgeRequest($request), $bridge_response, $is_authorized, 'testuser');
        return BridgeHelper::bridgeResponse($bridge_response);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function postToken(Request $request, Response $response) {
        $bridge_response = new BridgeResponse();
        try {
            $this->server->handleTokenRequest(BridgeHelper::bridgeRequest($request), $bridge_response);
            return BridgeHelper::bridgeResponse($bridge_response);
        } catch (EmailNotVerifiedException $e) {
            return $response->withJson(new ErrorHelper('postToken', 'End users email has not been verified'))->withStatus(401);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function getUserInfo(Request $request, Response $response) {
        $bridge_response = new BridgeResponse();
        $this->server->handleUserInfoRequest(BridgeHelper::bridgeRequest($request), $bridge_response);
        return BridgeHelper::bridgeResponse($bridge_response);
    }
}
