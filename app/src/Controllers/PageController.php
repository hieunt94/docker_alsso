<?php
namespace App\Controllers;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class PageController
 *
 * @package App\Controllers
 */
class PageController {
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Twig
     */
    private $view;

    /**
     * OAuth2Controller constructor.
     * @param Container $container
     */
    function __construct(Container $container) {
        $this->logger = $container->get('logger');
        $this->view = $container->get('view');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    function getHome(Request $request, Response $response) {
        return $this->view->render($response, 'home.twig');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    function getAngularView(Request $request, Response $response) {
        return $this->view->render($response, 'angular_view.twig');
    }
}