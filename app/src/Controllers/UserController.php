<?php
namespace App\Controllers;

use App\Helpers\BridgeHelper;
use App\Helpers\DataHelper;
use App\Helpers\ErrorHelper;
use App\Models\Map\UserTableMap;
use App\Models\User;
use App\Models\UserQuery;
use App\Services\AuthService;
use App\Services\MailService;
use App\Services\UserService;
use Aws\S3\S3Client;
use OAuth2\Server;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Propel;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class UserController
 *
 * @package App\Controllers
 */
class UserController {
    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var MailService
     */
    private $mailService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var Server
     */
    private $oauth2Server;

    /**
     * @var mixed
     */
    private $settings;

    /**
     * OAuth2Controller constructor.
     *
     * @param Container $container
     */
    function __construct(Container $container) {
        $this->authService = $container->get('auth_service');
        $this->mailService = $container->get('mail_service');
        $this->userService = $container->get('user_service');
        $this->oauth2Server = $container->get('oauth2_server');
        $this->settings = $container->get('settings');
    }

    /**
     * @param $params
     * @return mixed
     */
    private function sanitizeSignupParams(array $params) {
        $fields = ['given_name', 'family_name', 'gender', 'email', 'password', 'picture'];
        foreach ($params as $key => $value) {
            if (!in_array($key, $fields)) {
                unset($params[$key]);
            }
        }
        $params['password'] = $this->authService->hashPassword($params['password']);
        return $params;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function postSignup(Request $request, Response $response) {
        $params = $request->getParsedBody();
        try {
            $user = UserQuery::create()->findOneByEmail($params['email']);
            if ($user) {
                return $response->withJson(new ErrorHelper('postSignupEmailExists', 'The email is already registered in the database'))->withStatus(403);
            }
            $user = new User();
            $params = $this->sanitizeSignupParams($params);
            $user->fromArray($params, TableMap::TYPE_FIELDNAME);
            $user->setUid($this->authService->generateUid());
            $user->setName($user->getFamilyName().' '.$user->getGivenName());
            $user->setUsername($user->getEmail());
            $user->setEmailVerified(false);
            $user->setScope('openid profile email address phone alt');
            $user->setVerificationKey(sha1(uniqid(mt_rand(), true)));
            $user->save();
            $this->mailService->sendTemplateMail($user->getEmail(), [
                'user_name' => $user->getName(),
                'url' => $this->settings['base_url']."/signup_completed?uid=".$user->getUid()."&verification_key=".$user->getVerificationKey()
            ]);
            return $response->withJson(new DataHelper(['name' => $user->getName()]));
        } catch (\Exception $e) {
            return $response->withJson(new ErrorHelper('postSignupException', $e->getMessage()))->withStatus(403);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function postEmailVerification(Request $request, Response $response) {
        $uid = $request->getParam('uid');
        $verification_key = $request->getParam('verification_key');
        $user = UserQuery::create()->findOneByUid($uid);
        if ($user && $verification_key == $user->getVerificationKey()) {
            $user->setEmailVerified(true);
            $user->setVerificationKey(null);
            $user->save();
            return $response->withJson(new DataHelper(['email' => $user->getEmail()]));
        } else {
            return $response->withJson(new ErrorHelper('postEmailVerification', 'Invalid parameters'))->withStatus(403);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function postPicture(Request $request, Response $response) {
        $bridge_request = BridgeHelper::bridgeRequest($request);
        if ($this->oauth2Server->verifyResourceRequest($bridge_request)) {
            $token_data = $this->oauth2Server->getAccessTokenData($bridge_request);
            $user = UserQuery::create()->findOneByUsername($token_data['user_id']);
            $connection = Propel::getConnection(UserTableMap::TABLE_NAME);
            $connection->beginTransaction();
            try {
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'ap-northeast-1',
                    'credentials' => [
                        'key' => $this->settings['s3']['access_key'],
                        'secret' => $this->settings['s3']['secret_key']
                    ]
                ]);
                $file = $_FILES['file'];
                if (!isset($file['error']) || is_array($file['error'])) {
                    throw new RuntimeException('Invalid parameters');
                }
                switch ($file['error']) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new RuntimeException('No file sent');
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new RuntimeException('Exceeded filesize limit');
                    default:
                        throw new RuntimeException('Unknown errors');
                }
                // FIXME add current filesize limit
                if ($file['size'] > 1000000) {
                    throw new RuntimeException('Exceeded filesize limit');
                }
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                $content_types = ['jpg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif'];
                $extension = array_search($finfo->file($file['tmp_name']), $content_types, true);
                if ($extension === false) {
                    throw new RuntimeException('Invalid file format.');
                }
                $key = 'users/'.$user->getId().'/'.md5(uniqid($file["name"], true)).".".$extension;
                $s3->putObject([
                    'Bucket' => 'alt-sso',
                    'SourceFile' => $file['tmp_name'],
                    'ContentType' => $content_types[$extension],
                    'Key' => $key,
                    'ACL' => 'public-read'
                ]);
                // Remove the old picture if it's defined
                if ($user->getPicture()) {
                    $s3->deleteMatchingObjects('alt-sso', $user->getPicture());
                }
                // Save a reference to the icon in the bot_script database
                $user->setPicture($key);
                $user->save();
                $connection->commit();
                $response->withJson(new DataHelper([
                    'src' => $key
                ]));
            } catch (\Exception $e) {
                $connection->rollBack();
                return $response->withJson(new ErrorHelper('postPictureException', $e->getMessage()))->withStatus(403);
            }
        } else {
            return $response->withJson(new ErrorHelper('postPictureUnauthorized', 'Unauthorized access'))->withStatus(403);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function getAttributeNames(Request $request, Response $response) {
        $bridge_request = BridgeHelper::bridgeRequest($request);
        if ($this->oauth2Server->verifyResourceRequest($bridge_request)) {
            return $response->withJson($this->userService->getAttributeNames());
        } else {
            return $response->withJson(new ErrorHelper('getAttributeNamesUnauthorized', 'Unauthorized access'))->withStatus(403);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function queryAttributeSets(Request $request, Response $response) {
        $bridge_request = BridgeHelper::bridgeRequest($request);
        if ($this->oauth2Server->verifyResourceRequest($bridge_request)) {
            $result = $this->userService->queryAttributeSets($request->getParams());
            if ($result instanceof ErrorHelper) {
                return $response->withJson($result)->withStatus(403);
            }
            return $response->withJson($result);
        } else {
            return $response->withJson(new ErrorHelper('queryAttributeSetsUnauthorized', 'Unauthorized access'))->withStatus(403);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function postAttributeSet(Request $request, Response $response) {
        $bridge_request = BridgeHelper::bridgeRequest($request);
        if ($this->oauth2Server->verifyResourceRequest($bridge_request)) {
            $token_data = $this->oauth2Server->getAccessTokenData($bridge_request);
            $user = UserQuery::create()->findOneByUsername($token_data['user_id']);
            $result = $this->userService->addAttributeSet($user, $request->getParsedBody());
            if ($result instanceof ErrorHelper) {
                return $response->withJson($result)->withStatus(403);
            }
            return $response->withJson($result);
        } else {
            return $response->withJson(new ErrorHelper('postAttributeSetUnauthorized', 'Unauthorized access'))->withStatus(403);
        }
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    private function sanitizeUpdateParams($user, array $params) {
        $fields = ['id', 'uid', 'email', 'email_verified', 'username', 'password', 'created_at'];
        foreach ($fields as $field) {
            unset($params[$field]);
        }
        if (!empty($params['old_password'])) {
            if (!$this->authService->validatePassword($user->toArray(), $params['old_password'])) {
                throw new \Exception('Password mismatch');
            }
            $params['password'] = $this->authService->hashPassword($params['new_password']);
        }
        return $params;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    function postUpdate(Request $request, Response $response) {
        $bridge_request = BridgeHelper::bridgeRequest($request);
        if ($this->oauth2Server->verifyResourceRequest($bridge_request)) {
            $token_data = $this->oauth2Server->getAccessTokenData($bridge_request);
            $user = UserQuery::create()->findOneByUsername($token_data['user_id']);
            $connection = Propel::getConnection(UserTableMap::DATABASE_NAME);
            $connection->beginTransaction();
            try {
                $params = $request->getParsedBody();
                $params = $this->sanitizeUpdateParams($user, $params);
                $user->fromArray($params, TableMap::TYPE_FIELDNAME);
                $user->save();
                // TODO modify this to only add attributes that have changed
                $this->userService->addAttributeSet($user, [
                    'attributes' => [
                        ['attribute_group' => 'user_info', 'attribute_name' => 'name', 'attribute_type' => 'string', 'attribute_value' => $user->getName()],
                        ['attribute_group' => 'user_info', 'attribute_name' => 'given_name', 'attribute_type' => 'string', 'attribute_value' => $user->getGivenName()],
                        ['attribute_group' => 'user_info', 'attribute_name' => 'family_name', 'attribute_type' => 'string', 'attribute_value' => $user->getFamilyName()],
                        ['attribute_group' => 'user_info', 'attribute_name' => 'gender', 'attribute_type' => 'string', 'attribute_value' => $user->getGender()],
                        ['attribute_group' => 'user_info', 'attribute_name' => 'locale', 'attribute_type' => 'string', 'attribute_value' => $user->getLocale()],
                        ['attribute_group' => 'user_info', 'attribute_name' => 'picture', 'attribute_type' => 'string', 'attribute_value' => $user->getPicture()],
                        ['attribute_group' => 'user_info', 'attribute_name' => 'updated_at', 'attribute_type' => 'timestamp', 'attribute_value' => $user->getModifiedAt()],
                        ['attribute_group' => 'user_info', 'attribute_name' => 'email', 'attribute_type' => 'string', 'attribute_value' => $user->getEmail()],
                        ['attribute_group' => 'user_info', 'attribute_name' => 'email_verified', 'attribute_type' => 'boolean', 'attribute_value' => $user->getEmailVerified()]
                    ],
                    'origin' => 'sso',
                    'direction' => 'in',
                    'locale' => 'en_EN',
                    'created_at' => date_create()->format('c')
                ]);
                $connection->commit();
                return $response->withJson($user->toArray(TableMap::TYPE_FIELDNAME));
            } catch (\Exception $e) {
                $connection->rollBack();
                return $response->withJson(new ErrorHelper('postUpdateException', $e->getMessage()))->withStatus(403);
            }
        } else {
            return $response->withJson(new ErrorHelper('postUpdateUnauthorized', 'Unauthorized access'))->withStatus(403);
        }
    }
}