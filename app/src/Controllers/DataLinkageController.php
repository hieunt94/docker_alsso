<?php
namespace App\Controllers;

use App\Helpers\ErrorHelper;
use App\Models\Base\UserAttributeValue;
use App\Models\Base\UserAttributeValueQuery;
use App\Services\SlackService;
use Monolog\Logger;
use Propel\Runtime\Map\TableMap;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Facebook\Facebook;
use Abraham\TwitterOAuth\TwitterOAuth;

class DataLinkageController {

    private $logger;

    private $authSrv;

    private $settings;

    private $slack;

    private $facebook;

    private $twitter;

    private $google;

    function __construct(Container $container) {
        $this->logger = $container->get('logger');
        $this->settings = $container->get('settings');
        $this->authSrv = $container->get('auth_service');
    }

    private function getSlack($clientToken) {
        $redirectUri = $this->settings['base_url'] . $this->settings['slack']['redirect_uri']
                            . $clientToken;
        return new SlackService([
            'clientId'                => $this->settings['slack']['client_id'],
            'clientSecret'            => $this->settings['slack']['client_secret'],
            'redirectUri'             => $redirectUri
        ]);
    }

    private function getFacebook() {
        return new Facebook([
            'app_id' => $this->settings['facebook']['app_id'],
            'app_secret' => $this->settings['facebook']['app_secret']
        ]);
    }

    private function getTwitter($authToken = null, $authSecretToken = null) {
        return new TwitterOAuth(
            $this->settings['twitter']['app_id'], 
            $this->settings['twitter']['app_secret'],
            $authToken,
            $authSecretToken
        );
    }

    private function getGoogle() {
        $client = new \Google_Client();
        $client->setApplicationName($this->settings['google']['app_name']);
        $client->setClientId($this->settings['google']['app_id']);
        $client->setClientSecret($this->settings['google']['app_secret']);
        $redirectUri = $this->settings['base_url'] . $this->settings['google']['redirect_uri'];
        $client->setRedirectUri($redirectUri);
        $client->setAccessType("offline");
        return $client;
    } 

    function getSlackLogin(Request $request, Response $response) {
        $params = $request->getQueryParams();
        if (!$user = $this->authSrv->getUser($params['alt_token'])) {
            return $response->withJson(new ErrorHelper('400', 'Invalid access_token'));
        }

        $slack = $this->getSlack($params['alt_token']);
        $options = ['state' => uniqid(), 'scope' => $this->settings['slack']['scopes']];
        $loginUrl = $slack->getAuthorizationUrl($options);
        return $response->withJson(['login_url' => $loginUrl]);
    }

    function getSlackData(Request $request, Response $response) {
        $params = $request->getQueryParams();
        if (!$user = $this->authSrv->getUser($params['alt_token'])) {
            return $response->withJson(new ErrorHelper('400', 'Invalid access_token'));
        }

        try {
            $slack = $this->getSlack($params['alt_token']);
            $token = $slack->getAccessToken('authorization_code', ['code' => $_GET['code']]);
            $userInfo = $slack->getResourceOwner($token);
            return $response->withRedirect('/user_info');
        } catch (Exception $e) {
            return $response->withJson(new ErrorHelper('DataLinkageController.getSlackData.exception', $e->getMessage()));
        }
    }

    function getFacebookLogin(Request $request, Response $response) {
        $params = $request->getQueryParams();
        if (!$user = $this->authSrv->getUser($params['alt_token'])) {
            return $response->withJson(new ErrorHelper('400', 'Invalid access_token'));
        }

        $facebook = $this->getFacebook();
        $helper = $facebook->getRedirectLoginHelper();
        $redirectUri = $this->settings['base_url'] . $this->settings['facebook']['redirect_uri']
                        . $params['alt_token'];
        $loginUrl = $helper->getLoginUrl($redirectUri, $this->settings['facebook']['scopes']);
        return $response->withJson(['login_url' => $loginUrl]);
    }

    function getFacebookData(Request $request, Response $response) {
        $params = $request->getQueryParams();
        if (!$user = $this->authSrv->getUser($params['alt_token'])) {
            return $response->withJson(new ErrorHelper('400', 'Invalid access_token'));
        }

        $facebook = $this->getFacebook();
        $helper = $facebook->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
            $fields = '/me?fields=' . implode(',', $this->settings['facebook']['fields']);
            $facebookRes = $facebook->get($fields, $accessToken);
            $userInfo = $facebookRes->getGraphUser()->asArray();
            return $response->withRedirect('/user_info');
        } catch (Exception $e) {
            return $response->withJson(new ErrorHelper('DataLinkageController.getFacebookData.exception', $e->getMessage()));
        }
    }

    function getTwitterLogin(Request $request, Response $response) {
        $params = $request->getQueryParams();
        if (!$user = $this->authSrv->getUser($params['alt_token'])) {
            return $response->withJson(new ErrorHelper('400', 'Invalid access_token'));
        }

        $twitter = $this->getTwitter();
        $redirectUri = $this->settings['base_url'] . $this->settings['twitter']['redirect_uri']
                            . $params['alt_token'];
        $request_token = $twitter->oauth('oauth/request_token', ['oauth_callback' => $redirectUri]);

        unset($_SESSION['twitter_oauth_token']);
        unset($_SESSION['twitter_oauth_token_secret']);
        switch ($twitter->getLastHttpCode()) {
            case 200:
                $_SESSION['twitter_oauth_token'] = $request_token['oauth_token'];
                $_SESSION['twitter_oauth_token_secret'] = $request_token['oauth_token_secret'];
                $loginUrl = $twitter->url('oauth/authorize', ['oauth_token' => $request_token['oauth_token']]);
                return $response->withJson(['login_url' => $loginUrl]);
            default:
                return $response->withJson(new ErrorHelper('400', 'Could not connect to Twitter. Refresh the page or try again later.'));
        }
    }

    function getTwitterData(Request $request, Response $response) {
        $params = $request->getQueryParams();
        if (!$user = $this->authSrv->getUser($params['alt_token'])) {
            return $response->withJson(new ErrorHelper('400', 'Invalid access_token'));
        }

        $twitter = $this->getTwitter($_SESSION['twitter_oauth_token'], $_SESSION['twitter_oauth_token_secret']);
        $access_token = $twitter->oauth("oauth/access_token", ["oauth_verifier" => $_GET['oauth_verifier']]);
        if (200 == $twitter->getLastHttpCode()) {
            $twitter = new TwitterOAuth(TWITTER_APP_ID, TWITTER_APP_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
            $userInfo = $twitter->get('account/verify_credentials');
            unset($_SESSION['twitter_oauth_token']);
            unset($_SESSION['twitter_oauth_token_secret']);
            return $response->withRedirect('/user_info');
        }
    }

    function getGoogleLogin(Request $request, Response $response) {
        $params = $request->getQueryParams();
        if (!$user = $this->authSrv->getUser($params['alt_token'])) {
            return $response->withJson(new ErrorHelper('400', 'Invalid access_token'));
        }

        $google = $this->getGoogle();
        $google->addScope(\Google_Service_Plus::USERINFO_EMAIL);
        $google->addScope(\Google_Service_Plus::USERINFO_PROFILE);
        $google->addScope(\Google_Service_Calendar::CALENDAR);
        $google->addScope(\Google_Service_Calendar::CALENDAR_READONLY);
        $google->addScope(\Google_Service_Gmail::MAIL_GOOGLE_COM);
        $google->addScope(\Google_Service_Gmail::GMAIL_READONLY);
        $google->addScope(\Google_Service_Gmail::GMAIL_MODIFY);
        $google->addScope(\Google_Service_Gmail::GMAIL_COMPOSE);
        $google->setState($params['alt_token']);

        $loginUrl = $google->createAuthUrl();
        return $response->withJson(['login_url' => $loginUrl]);
    }

    function getGoogleData(Request $request, Response $response) {
        $params = $request->getQueryParams();
        if (!$user = $this->authSrv->getUser($params['state'])) {
            return $response->withJson(new ErrorHelper('400', 'Invalid access_token'));
        }

        try {
            $google = $this->getGoogle();
            $google->authenticate($params['code']);
            $google->setAccessToken($google->getAccessToken());
            $plus = new \Google_Service_Plus($google);
            $userInfo = $plus->people->get('me')->toSimpleObject();

            unset($_SESSION['access_token']);
            return $response->withRedirect('/user_info');
        } catch (Exception $e) {
            return $response->withJson(new ErrorHelper('DataLinkageController.getGoogleData.exception', $e->getMessage()));
        }
    }
}