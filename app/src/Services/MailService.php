<?php
namespace App\Services;

use Psr\Log\LoggerInterface;

class MailService {
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct($c) {
        $this->logger = $c->get('logger');
    }

    function sendMail($to, $subject, $body) {
        $transport = \Swift_SmtpTransport::newInstance('alt-inc.sakura.ne.jp', 25)
            ->setUsername('sso@alt.ai')
            ->setPassword('4hzQ7Wi3Kr3f');
        $mailer = \Swift_Mailer::newInstance($transport);
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom(['no-reply@alt.ai' => 'al+ SSO Account'])
            ->setBody($body);
        $this->logger->info('Message sent to '.$to);
        return $mailer->send($message);
    }

    function sendTemplateMail($to, $params = []) {
        $params = array_merge($params, ['service_name' => 'Al+ SSO']);
        $subject = "al+ SSO Account メール認証のお知らせ";
        $body = "{user_name}様\nal+ SSO Account にお申し込み頂き、ありがとうございます。\n下記URLにアクセスしメール認証を行って下さい。\n{url}\n認証後ログイン可能となりますのでよろしくお願いします。\n\n+++++++++++++++++++++++++++++\nal+ SSO Account\n会員登録事務局\n+++++++++++++++++++++++++++++";
        foreach ($params as $key => $val) {
            $body = str_replace("{".$key."}", $val, $body);
            $subject = str_replace("{".$key."}", $val, $subject);
        }
        return $this->sendMail($to, $subject, $body);
    }
}