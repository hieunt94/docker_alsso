<?php
namespace App\Services;

use App\Helpers\ErrorHelper;
use App\Models\Label;
use App\Models\LabelGroup;
use App\Models\LabelGroupQuery;
use App\Models\LabelOrigin;
use App\Models\LabelOriginQuery;
use App\Models\LabelQuery;
use App\Models\LabelSet;
use App\Models\Map\MessageLabelValueTableMap;
use App\Models\Message;
use App\Models\MessageLabelValue;
use App\Models\MessageLabelValueQuery;
use Monolog\Logger;
use Propel\Runtime\Propel;
use Slim\Container;

/**
 * Class MessageService
 * @package App\Services
 */
class MessageService {
    /**
     * @var Logger
     */
    private $logger;

    /**
     * UserService constructor.
     *
     * @param Container $container
     */
    function __construct(Container $container) {
        $this->logger = $container->get('logger');
    }

    /**
     * @param $keys
     * @param array $array
     * @return bool
     */
    private function arrayKeyExists($keys, array $array) {
        foreach ($keys as $key) {
            if (!array_key_exists($key, $array)) return false;
        }
        return true;
    }

    /**
     * @param Message $message
     * @param $parsed_body
     * @return mixed
     */
    function addLabelSet(Message $message, $parsed_body) {
        $group_buffer = [];
        $label_buffer = [];
        if ($this->arrayKeyExists(['labels', 'origin', 'created_at'], $parsed_body)) {
            if (is_array($parsed_body['labels'])) {
                $this->logger->info(sprintf('Aggregating %d labels from %s...', count($parsed_body['labels']), $parsed_body['origin']));
                $origin = LabelOriginQuery::create()->findOneByLabelOriginName($parsed_body['origin']);
                if (!$origin) {
                    $origin = new LabelOrigin();
                    $origin->setLabelOriginName($parsed_body['origin']);
                }
                $set = new LabelSet();
                $set->setLabelOrigin($origin);
                $set->setCreatedAt(new \DateTime());
                foreach ($parsed_body['labels'] as $r) {
                    if ($this->arrayKeyExists(['label_group', 'label_name', 'label_type', 'label_value'], $r)) {
                        if (!array_key_exists($r['label_group'], $group_buffer)) {
                            $group = LabelGroupQuery::create()->findOneByLabelGroupName($r['label_group']);
                            if (!$group) {
                                $group = new LabelGroup();
                                $group->setLabelGroupName($r['label_group']);
                            }
                            $group_buffer[$r['label_group']] = $group;
                            $label_buffer[$r['label_group']] = [];
                        }
                        if (!array_key_exists($r['label_name'], $label_buffer[$r['label_group']])) {
                            $label = LabelQuery::create()->findOneByLabelName($r['label_name']);
                            if (!$label) {
                                $label = new Label();
                                $label->setLabelName($r['label_name']);
                                $label->setLabelGroup($group_buffer[$r['label_group']]);
                                if (!in_array($r['label_type'], ['boolean', 'double', 'string', 'timestamp'])) {
                                    return new ErrorHelper('postLabelSetError', 'Unsupported label type '.$r['label_type']);
                                }
                                $label->setLabelType($r['label_type']);
                                // TODO define when multiple is allowed
                                $label->setIsMultipleAllowed(false);
                            }
                            $label_buffer[$r['label_group']][$r['label_name']] = $label;
                        }
                        $value = new MessageLabelValue();
                        $value->setMessage($message);
                        $value->setLabel($label_buffer[$r['label_group']][$r['label_name']]);
                        $value->setLabelSet($set);
                        switch ($label_buffer[$r['label_group']][$r['label_name']]->getLabelType()) {
                            case 'boolean':
                                $value->setBooleanValue($r['label_value']);
                                break;
                            case 'double':
                                $value->setDoubleValue($r['label_value']);
                                break;
                            case 'string':
                                $value->setStringValue($r['label_value']);
                                break;
                            case 'timestamp':
                                $value->setTimestampValue($r['label_value']);
                                break;
                        }
                        $value->setCreatedAt(new \DateTime());
                    } else {
                        return new ErrorHelper('postLabelSetError', 'Request error');
                    }
                }
                $set->save();
                $this->logger->info('...end');
                return ['set_id' => $set->getId()];
            } else {
                return new ErrorHelper('postLabelSetEmpty', 'Empty label set');
            }
        } else {
            return new ErrorHelper('postLabelSetError', 'Request error');
        }
    }

    /**
     * @return array
     */
    function getLabelNames() {
        $labels = [];
        foreach (LabelGroupQuery::create()->find() as $group) {
            foreach (LabelQuery::create()->findByLabelGroupId($group->getId()) as $label) {
                $labels[$group->getLabelGroupName()][] = $label->getLabelName();
            }
        }
        return $labels;
    }

    /**
     * @param $params
     * @return ErrorHelper|array
     * @throws \Exception
     */
    function queryLabelSets($params) {
        if ($this->arrayKeyExists(['label_groups', 'label_names', 'method'], $params)) {
            $groups = [];
            foreach (LabelGroupQuery::create()->findByLabelGroupName($params['label_groups']) as $group) {
                $groups[] = $group->getId();
            }
            $labels = [];
            if ($params['label_names']) {
                foreach (LabelQuery::create()->findByLabelName($params['label_names']) as $label) {
                    $labels[] = $label->getId();
                }
            } else {
                foreach (LabelQuery::create()->findByLabelGroupId($groups) as $label) {
                    $labels[] = $label->getId();
                }
            }
            $sets = [];
            if ($params['method'] == 'raw') {
            } else if ($params['method'] == 'latest') {
                $connection = Propel::getWriteConnection(MessageLabelValueTableMap::DATABASE_NAME);
                $sql = "SELECT v1.id FROM user_label_values AS v1 JOIN labels AS a ON v1.label_id = a.id ".
                    "JOIN (SELECT i.label_id, MAX(i.created_at) as created_at FROM user_label_values AS i GROUP BY i.label_id) AS v2 ON v1.label_id = v2.label_id AND v1.created_at = v2.created_at ".
                    "WHERE v1.label_id IN (".implode(', ', $labels).") ORDER BY a.label_name";
                $stmt = $connection->prepare($sql);
                $stmt->execute();
                while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                    // TODO fix this to use a Propel data formatter or only PDO
                    $label_value = MessageLabelValueQuery::create()->findOneById($row['id']);
                    $label = $label_value->getLabel();
                    switch ($label->getLabelType()) {
                        case 'boolean':
                            $value = $label_value->getBooleanValue();
                            break;
                        case 'string':
                            $value = $label_value->getStringValue();
                            break;
                        case 'double':
                            $value = $label_value->getDoubleValue();
                            break;
                        case 'timestamp':
                            $value = $label_value->getTimestampValue()->format('c');
                            break;
                        default:
                            throw new \Exception();
                    }
                    $sets[$label_value->getLabelSetId()][] = [
                        'label_group' => $label->getLabelGroup()->getLabelGroupName(),
                        'label_name' => $label->getLabelName(),
                        'label_type' => $label->getLabelType(),
                        'label_value' => $value,
                        'created_at' => $label_value->getCreatedAt()->format('c'),
                        'origin' => $label_value->getLabelSet()->getLabelOrigin()->getLabelOriginName()
                    ];
                }
                return ['sets' => array_values($sets)];
            }
        }
        return new ErrorHelper('queryLabelSets', 'Invalid parameters');
    }
}