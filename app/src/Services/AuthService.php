<?php
namespace App\Services;

use App\Helpers\BridgeHelper;
use App\Models\Base\AccessTokenQuery;
use App\Models\User;
use App\Models\UserQuery;
use OAuth2\Request;
use OAuth2\Server;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use OAuth2\HttpFoundationBridge\Response as BridgeResponse;

/**
 * Class AuthService
 *
 * @package App\Services
 */
class AuthService {
    /**
     * @var int
     */
    private $uidDef;

    /**
     * @var Server
     */
    private $oauth2Server;

    /**
     * AuthService constructor.
     *
     * @param Container $container
     */
    function __construct(Container $container) {
        $this->uidDef = $container->get('settings')['uidDef'];
        $this->oauth2Server = $container->get('oauth2_server');
    }

    /**
     * @param $accessToken
     * @return User|bool
     */
    function getUser($accessToken) {
        $token = AccessTokenQuery::create()->findOneByAccessToken($accessToken);
        if (empty($token) || time() > $token->getExpires()->getTimestamp()) {
            return false;
        }
        return UserQuery::create()->findOneByUsername($token->getUserId());
    }

    /**
     * @param $username
     * @param $password
     * @return ResponseInterface
     */
    function handleTokenRequest($username, $password) {
        $bridge_request = new Request([], [
            'client_id' => 'server',
            'grant_type' => 'password',
            'username' => $username,
            'password' => $password
        ], [], [], [], ['REQUEST_METHOD' => 'POST']);
        $bridge_response = new BridgeResponse();
        $this->oauth2Server->getTokenController()->handleTokenRequest($bridge_request, $bridge_response);
        return BridgeHelper::bridgeResponse($bridge_response);
    }

    /**
     * @return string
     */
    function generateUid() {
        $query = UserQuery::create();
        do {
            $uid = md5(time() + $this->uidDef + rand());
            $user = $query->findOneByUid($uid);
            if (is_null($user)) {
                break;
            }
        } while (true);
        return $uid;
    }

    /**
     * @param $password
     * @return string
     */
    function hashPassword($password) {
        $target = 0.05;
        $cost = 8;
        do {
            $cost++;
            $start = microtime(true);
            $hash = password_hash($password, PASSWORD_BCRYPT, ["cost" => $cost]);
            // password_hash sanity check
            if (!$hash) throw new \LogicException();
            $end = microtime(true);
        } while (($end - $start) < $target);
        return $hash;
    }

    /**
     * @param $user
     * @param $password
     * @return bool
     */
    function validatePassword($user, $password) {
        return password_verify($password, $user['password']);
    }
}