<?php
namespace App\Services;

use App\Helpers\ErrorHelper;
use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\AttributeGroupQuery;
use App\Models\AttributeOrigin;
use App\Models\AttributeOriginQuery;
use App\Models\AttributeQuery;
use App\Models\AttributeSet;
use App\Models\Map\UserAttributeValueTableMap;
use App\Models\User;
use App\Models\UserAttributeValue;
use App\Models\UserAttributeValueQuery;
use Monolog\Logger;
use Propel\Runtime\Propel;
use Slim\Container;

/**
 * Class UserService
 *
 * @package App\Services
 */
class UserService {
    /**
     * @var Logger
     */
    private $logger;

    /**
     * UserService constructor.
     *
     * @param Container $container
     */
    function __construct(Container $container) {
        $this->logger = $container->get('logger');
    }

    /**
     * @param $keys
     * @param array $array
     * @return bool
     */
    private function arrayKeyExists($keys, array $array) {
        foreach ($keys as $key) {
            if (!array_key_exists($key, $array)) return false;
        }
        return true;
    }

    /**
     * Update user info from attribute set
     *
     * Attribute group user_info conforms to the OpenId Connect standard.
     *
     * @param AttributeSet $set
     */
    private function updateUserInfo(AttributeSet $set) {
        foreach ($set->getUserAttributeValues() as $attribute_value) {
            $user = $attribute_value->getUser();
            $modified_at = $user->getModifiedAt();
            if ($attribute_value->getAttribute()->getAttributeGroup()->getAttributeGroupName() == 'user_info') {
                switch ($attribute_value->getAttribute()->getAttributeName()) {
                    case 'name':
                        $user->setName($attribute_value->getStringValue());
                        break;
                    case 'given_name':
                        $user->setGivenName($attribute_value->getStringValue());
                        break;
                    case 'family_name':
                        $user->setFamilyName($attribute_value->getStringValue());
                        break;
                    case 'middle_name':
                        $user->setMiddleName($attribute_value->getStringValue());
                        break;
                    case 'nickname':
                        $user->setNickname($attribute_value->getStringValue());
                        break;
                    case 'preferred_username':
                        $user->setPreferredUsername($attribute_value->getStringValue());
                        break;
                    case 'profile':
                        $user->setProfile($attribute_value->getStringValue());
                        break;
                    case 'picture':
                        $user->setPicture($attribute_value->getStringValue());
                        break;
                    case 'website':
                        $user->setWebsite($attribute_value->getStringValue());
                        break;
                    case 'email':
                        $user->setEmail($attribute_value->getStringValue());
                        break;
                    case 'email_verified':
                        $user->setEmailVerified($attribute_value->getBooleanValue());
                        break;
                    case 'gender':
                        $user->setGender($attribute_value->getStringValue());
                        break;
                    case 'birthdate':
                        $user->setBirthDate($attribute_value->getTimestampValue());
                        break;
                    case 'zoneinfo':
                        $user->setZoneinfo($attribute_value->getStringValue());
                        break;
                    case 'locale':
                        $user->setLocale($attribute_value->getStringValue());
                        break;
                    case 'phone_number':
                        $user->setPhoneNumber($attribute_value->getStringValue());
                        break;
                    case 'phone_number_verified':
                        $user->setPhoneNumberVerified($attribute_value->getStringValue());
                        break;
                    case 'address':
                        $user->setAddress($attribute_value->getStringValue());
                        break;
                    case 'updated_at':
                        $user->setModifiedAt($attribute_value->getTimestampValue());
                        break;
                }
            }
            // Save only if the received information is more recent
            if ($modified_at < $user->getModifiedAt()) {
                $user->save();
            }
        }
    }

    /**
     * @param User $user
     * @param $parsed_body
     * @return mixed
     */
    function addAttributeSet(User $user, $parsed_body) {
        $group_buffer = [];
        $attribute_buffer = [];
        if ($this->arrayKeyExists(['attributes', 'origin', 'created_at'], $parsed_body)) {
            if (is_array($parsed_body['attributes'])) {
                $this->logger->info(sprintf('Aggregating %d attributes from %s...', count($parsed_body['attributes']), $parsed_body['origin']));
                $origin = AttributeOriginQuery::create()->findOneByAttributeOriginName($parsed_body['origin']);
                if (!$origin) {
                    $origin = new AttributeOrigin();
                    $origin->setAttributeOriginName($parsed_body['origin']);
                }
                $set = new AttributeSet();
                $set->setAttributeOrigin($origin);
                $set->setCreatedAt(new \DateTime());
                foreach ($parsed_body['attributes'] as $r) {
                    if ($this->arrayKeyExists(['attribute_group', 'attribute_name', 'attribute_type', 'attribute_value'], $r)) {
                        if (!array_key_exists($r['attribute_group'], $group_buffer)) {
                            $group = AttributeGroupQuery::create()->findOneByAttributeGroupName($r['attribute_group']);
                            if (!$group) {
                                $group = new AttributeGroup();
                                $group->setAttributeGroupName($r['attribute_group']);
                            }
                            $group_buffer[$r['attribute_group']] = $group;
                            $attribute_buffer[$r['attribute_group']] = [];
                        }
                        if (!array_key_exists($r['attribute_name'], $attribute_buffer[$r['attribute_group']])) {
                            $attribute = AttributeQuery::create()->findOneByAttributeName($r['attribute_name']);
                            if (!$attribute) {
                                $attribute = new Attribute();
                                $attribute->setAttributeName($r['attribute_name']);
                                $attribute->setAttributeGroup($group_buffer[$r['attribute_group']]);
                                if (!in_array($r['attribute_type'], ['boolean', 'double', 'string', 'timestamp'])) {
                                    return new ErrorHelper('postAttributeSetError', 'Unsupported attribute type '.$r['attribute_type']);
                                }
                                $attribute->setAttributeType($r['attribute_type']);
                                // TODO define when multiple is allowed
                                $attribute->setIsMultipleAllowed(false);
                            }
                            $attribute_buffer[$r['attribute_group']][$r['attribute_name']] = $attribute;
                        }
                        $value = new UserAttributeValue();
                        $value->setUser($user);
                        $value->setAttribute($attribute_buffer[$r['attribute_group']][$r['attribute_name']]);
                        $value->setAttributeSet($set);
                        switch ($attribute_buffer[$r['attribute_group']][$r['attribute_name']]->getAttributeType()) {
                            case 'boolean':
                                $value->setBooleanValue($r['attribute_value']);
                                break;
                            case 'double':
                                $value->setDoubleValue($r['attribute_value']);
                                break;
                            case 'string':
                                $value->setStringValue($r['attribute_value']);
                                break;
                            case 'timestamp':
                                $value->setTimestampValue($r['attribute_value']);
                                break;
                        }
                        $value->setCreatedAt(new \DateTime());
                    } else {
                        return new ErrorHelper('postAttributeSetError', 'Request error');
                    }
                }
                $set->save();
                $this->updateUserInfo($set);
                $this->logger->info('...end');
                return ['set_id' => $set->getId()];
            } else {
                return new ErrorHelper('postAttributeSetEmpty', 'Empty attribute set');
            }
        } else {
            return new ErrorHelper('postAttributeSetError', 'Request error');
        }
    }

    /**
     * @return array
     */
    function getAttributeNames() {
        $attributes = [];
        foreach (AttributeGroupQuery::create()->find() as $group) {
            foreach (AttributeQuery::create()->findByAttributeGroupId($group->getId()) as $attribute) {
                $attributes[$group->getAttributeGroupName()][] = $attribute->getAttributeName();
            }
        }
        return $attributes;
    }

    /**
     * @param $params
     * @return ErrorHelper|array
     * @throws \Exception
     */
    function queryAttributeSets($params) {
        if ($this->arrayKeyExists(['attribute_groups', 'attribute_names', 'method'], $params)) {
            $groups = [];
            foreach (AttributeGroupQuery::create()->findByAttributeGroupName($params['attribute_groups']) as $group) {
                $groups[] = $group->getId();
            }
            $attributes = [];
            if ($params['attribute_names']) {
                foreach (AttributeQuery::create()->findByAttributeName($params['attribute_names']) as $attribute) {
                    $attributes[] = $attribute->getId();
                }
            } else {
                foreach (AttributeQuery::create()->findByAttributeGroupId($groups) as $attribute) {
                    $attributes[] = $attribute->getId();
                }
            }
            $sets = [];
            if ($params['method'] == 'raw') {
            } else if ($params['method'] == 'latest') {
                $connection = Propel::getWriteConnection(UserAttributeValueTableMap::DATABASE_NAME);
                $sql = "SELECT v1.id FROM user_attribute_values AS v1 JOIN attributes AS a ON v1.attribute_id = a.id ".
                    "JOIN (SELECT i.attribute_id, MAX(i.created_at) as created_at FROM user_attribute_values AS i GROUP BY i.attribute_id) AS v2 ON v1.attribute_id = v2.attribute_id AND v1.created_at = v2.created_at ".
                    "WHERE v1.attribute_id IN (".implode(', ', $attributes).") ORDER BY a.attribute_name";
                $stmt = $connection->prepare($sql);
                $stmt->execute();
                while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                    // TODO fix this to use a Propel data formatter or only PDO
                    $attribute_value = UserAttributeValueQuery::create()->findOneById($row['id']);
                    $attribute = $attribute_value->getAttribute();
                    switch ($attribute->getAttributeType()) {
                        case 'boolean':
                            $value = $attribute_value->getBooleanValue();
                            break;
                        case 'string':
                            $value = $attribute_value->getStringValue();
                            break;
                        case 'double':
                            $value = $attribute_value->getDoubleValue();
                            break;
                        case 'timestamp':
                            $value = $attribute_value->getTimestampValue()->format('c');
                            break;
                        default:
                            throw new \Exception('Unknown type '.$attribute->getAttributeType());
                    }
                    $sets[$attribute_value->getAttributeSetId()][] = [
                        'attribute_group' => $attribute->getAttributeGroup()->getAttributeGroupName(),
                        'attribute_name' => $attribute->getAttributeName(),
                        'attribute_type' => $attribute->getAttributeType(),
                        'attribute_value' => $value,
                        'created_at' => $attribute_value->getCreatedAt()->format('c'),
                        'origin' => $attribute_value->getAttributeSet()->getAttributeOrigin()->getAttributeOriginName()
                    ];
                }
                return ['sets' => array_values($sets)];
            }
        }
        return new ErrorHelper('queryAttributeSets', 'Invalid parameters');
    }
}