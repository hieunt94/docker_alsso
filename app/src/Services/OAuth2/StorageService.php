<?php
namespace App\Services\OAuth2;

use OAuth2\Storage\Pdo;

/**
 * Class StorageService
 *
 * @package App\Services\OAuth2
 */
class StorageService extends Pdo {
    const VALID_CLAIMS = 'profile email address phone alt';

    const ALT_CLAIM_VALUES = 'uid';

    private $field_conversions = ['modified' => 'updated_at'];

    /**
     * StorageService constructor.
     *
     * @param $connection
     */
    public function __construct($connection) {
        parent::__construct($connection, [
            'client_table' => 'clients',
            'access_token_table' => 'access_tokens',
            'refresh_token_table' => 'refresh_tokens',
            'code_table' => 'authorization_codes',
            'user_table' => 'users',
            'jwt_table' => 'jwt',
            'jti_table' => 'jti',
            'scope_table' => 'scopes',
            'public_key_table' => 'public_keys',
        ]);
    }

    /**
     * @param $user
     * @param $password
     * @return bool
     * @throws \Exception
     */
    protected function checkPassword($user, $password) {
        if ($user['email_verified']) {
            return password_verify($password, $user['password']);
        }
        throw new EmailNotVerifiedException();
    }

    public function getUserClaims($user_id, $claims) {
        if (!$userDetails = $this->getUserDetails($user_id)) {
            return false;
        }

        $claims = explode(' ', trim($claims));
        $userClaims = array();

        // for each requested claim, if the user has the claim, set it in the response
        $validClaims = explode(' ', self::VALID_CLAIMS);
        foreach ($validClaims as $validClaim) {
            if (in_array($validClaim, $claims)) {
                if ($validClaim == 'address') {
                    // address is an object with subfields
                    $userClaims['address'] = $this->getUserClaim($validClaim, $userDetails['address'] ?: $userDetails);
                } else {
                    $userClaims = array_merge($userClaims, $this->getUserClaim($validClaim, $userDetails));
                }
            }
        }

        return $userClaims;
    }

    protected function getUserClaim($claim, $userDetails) {
        $userClaims = array();
        $claimValuesString = constant(sprintf('self::%s_CLAIM_VALUES', strtoupper($claim)));
        $claimValues = explode(' ', $claimValuesString);

        foreach ($claimValues as $value) {
            $key = array_key_exists($value, $this->field_conversions) ? $this->field_conversions[$value] : $value;
            $userClaims[$value] = isset($userDetails[$key]) ? $userDetails[$key] : null;
        }

        return $userClaims;
    }
}