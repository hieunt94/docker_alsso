<?php
namespace App\Models;

use App\Models\Base\PublicKey as BasePublicKey;

/**
 * Skeleton subclass for representing a row from the 'public_keys' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PublicKey extends BasePublicKey {

}
