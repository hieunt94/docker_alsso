<?php

namespace App\Models;

use App\Models\Base\LabelOriginQuery as BaseLabelOriginQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'label_origins' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class LabelOriginQuery extends BaseLabelOriginQuery
{

}
