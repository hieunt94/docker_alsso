<?php

namespace App\Models;

use App\Models\Base\UserAttributeSet as BaseUserAttributeSet;

/**
 * Skeleton subclass for representing a row from the 'user_attribute_value_sets' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserAttributeSet extends BaseUserAttributeSet
{

}
