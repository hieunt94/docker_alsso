<?php
namespace App\Models;

use App\Models\Base\TmpUserQuery as BaseTmpUserQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'tmp_users' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TmpUserQuery extends BaseTmpUserQuery {

}
