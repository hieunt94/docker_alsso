<?php
namespace App\Helpers;

/**
 * Class ResponseHelper
 *
 * @package App\Helpers
 */
class DataHelper {
    /**
     * @var array
     */
    public $data;

    /**
     * ResponseHelper constructor.
     *
     * @param array $data
     */
    public function __construct(array $data) {
        $this->data = $data;
    }
}