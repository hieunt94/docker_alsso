<?php
namespace App\Helpers;

use OAuth2\HttpFoundationBridge\Request as BridgeRequest;
use OAuth2\HttpFoundationBridge\Response as BridgeResponse;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;

/**
 * Class BridgeHelper
 *
 * @package App\Helpers
 */
class BridgeHelper {
    /**
     * Convert request from PSR to HttpFoundation and from there to OAuth2
     *
     * @param RequestInterface $request
     * @return BridgeRequest
     */
    static function bridgeRequest(RequestInterface $request) {
        $factory = new HttpFoundationFactory();
        $foundation_request = $factory->createRequest($request);
        return BridgeRequest::createFromRequest($foundation_request);
    }

    /**
     * Convert response from HttpFoundation to PSR
     *
     * @param BridgeResponse $response
     * @return ResponseInterface
     */
    static function bridgeResponse(BridgeResponse $response) {
        $factory = new DiactorosFactory();
        return $factory->createResponse($response);
    }
}