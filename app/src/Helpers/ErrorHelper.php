<?php
namespace App\Helpers;

/**
 * Class ErrorHelper
 *
 * @package App\Helpers
 */
class ErrorHelper {
    /**
     * @var string
     */
    public $error;

    /**
     * @var string
     */
    public $error_description;

    /**
     * ErrorHelper constructor.
     *
     * @param $error
     * @param $error_description
     */
    public function __construct($error, $error_description) {
        $this->error = $error;
        $this->error_description = $error_description;
    }
}