<?php
$container = $app->getContainer();

/**
 * Cross-site request forgery check
 */
$app->add(function ($request, $response, $next) {
    // TODO add CSRF middleware
    $response = $next($request, $response);
    return $response;
});