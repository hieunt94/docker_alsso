<?php
use App\Controllers\AuthController;
use App\Controllers\MessageController;
use App\Controllers\UserController;
use App\Controllers\PageController;
use App\Controllers\DataLinkageController;

$container = $app->getContainer();

/**
 * This is the homepage route, the homepage is generated and cached server side and doesn't use Angular.
 */
$app->get('/', PageController::class.':getHome');

/**
 * These routes mirror the Angular app routes, they are used for better 404 support.
 * This routes can also be used to get better SEO using server side view generation.
 */
$app->get('/login', PageController::class.':getAngularView');
$app->get('/signup', PageController::class.':getAngularView');
$app->get('/signup_completed', PageController::class.':getAngularView');
$app->get('/signup_verification', PageController::class.':getAngularView');
$app->get('/signup_email_completed', PageController::class.':getAngularView');
$app->get('/account', PageController::class.':getAngularView');
$app->get('/items', PageController::class.':getAngularView');
$app->get('/avatar', PageController::class.':getAngularView');
$app->get('/profile', PageController::class.':getAngularView');
$app->get('/data_linkage', PageController::class.':getAngularView');
$app->get('/friends', PageController::class.':getAngularView');
$app->get('/terms_of_use', PageController::class.':getAngularView');
$app->get('/error', PageController::class.':getAngularView');
$app->get('/debug', PageController::class.':getAngularView');

/**
 * These routes are the users, messages, oauth2 and data_linkage.
 * The oauth2 routes conform to the OAuth2 and OpenID Connect specification.
 */
$app->group('/api', function () {
    $this->group('/users', function () {
        $this->post('/signup', UserController::class.':postSignup');
        $this->post('/verify/email', UserController::class.':postEmailVerification');
        $this->post('/picture', UserController::class.':postPicture');
        $this->post('/update', UserController::class.':postUpdate');
        $this->get('/attribute_names', UserController::class.':getAttributeNames');
        $this->post('/attribute_sets/query', UserController::class.':queryAttributeSets');
        $this->post('/attribute_sets', UserController::class.':postAttributeSets');

    });
    $this->group('/messages', function () {
        $this->get('/label_names', MessageController::class.':getLabelNames');
        $this->post('/label_sets/query', MessageController::class.':queryLabelSets');
        $this->post('/label_sets', MessageController::class.':postLabelSets');
    });
    $this->group('/oauth2', function () {
        $this->get('/authorize', AuthController::class.':getAuthorize');
        $this->post('/authorize', AuthController::class.':postAuthorize');
        $this->post('/token', AuthController::class.':postToken');
        $this->get('/user_info', AuthController::class.':getUserInfo');
    });
    $this->group('/data_linkage', function () {
        $this->get('/slack_login', DataLinkageController::class.':getSlackLogin');
        $this->get('/slack_data', DataLinkageController::class.':getSlackData');
        $this->get('/facebook_login', DataLinkageController::class.':getFacebookLogin');
        $this->get('/facebook_data', DataLinkageController::class.':getFacebookData');
        $this->get('/twitter_login', DataLinkageController::class.':getTwitterLogin');
        $this->get('/twitter_data', DataLinkageController::class.':getTwitterData');
        $this->get('/google_login', DataLinkageController::class.':getGoogleLogin');
        $this->get('/google_data', DataLinkageController::class.':getGoogleData');
    });
});