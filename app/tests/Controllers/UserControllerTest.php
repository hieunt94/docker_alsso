<?php
namespace Tests\Controllers;

/**
 * Class RegistrationControllerTest
 * @package Tests\Controllers
 */
class UserControllerTest extends ControllerTestCase {
    public function testGetAttributeNames() {
        // Get a access token for the next request
        $response = $this->mockRequest('POST', '/api/oauth2/token', [
            'grant_type' => 'password',
            'client_id' => 'server',
            'username' => 'test@test.com',
            'password' => 'test'
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $response_body = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('access_token', $response_body);
        $this->accessToken = $response_body['access_token'];
        // Get the latest user_info data
        $response = $this->mockRequest('GET', '/api/users/attribute_names');
        $this->assertEquals(200, $response->getStatusCode());
        $parsed_response = json_decode($response->getBody(), true);
        $this->assertEquals([], $parsed_response);
    }

    public function testPostAttributeSet() {
        // Get a access token for the next request
        $response = $this->mockRequest('POST', '/api/oauth2/token', [
            'grant_type' => 'password',
            'client_id' => 'server',
            'username' => 'test@test.com',
            'password' => 'test'
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $response_body = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('access_token', $response_body);
        $this->accessToken = $response_body['access_token'];
        // Post a attribute set as a user
        $response = $this->mockRequest('POST', '/api/users/attribute_sets', [
            'attributes' => [
                ['attribute_group' => 'user_info', 'attribute_name' => 'name', 'attribute_type' => 'string', 'attribute_value' => 'Marcos Cooper'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'given_name', 'attribute_type' => 'string', 'attribute_value' => 'Marcos'], // facebook -> first_name
                ['attribute_group' => 'user_info', 'attribute_name' => 'family_name', 'attribute_type' => 'string', 'attribute_value' => 'Cooper'], // facebook -> last_name
                ['attribute_group' => 'user_info', 'attribute_name' => 'gender', 'attribute_type' => 'string', 'attribute_value' => 'Male'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'locale', 'attribute_type' => 'string', 'attribute_value' => 'ja_JP'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'picture', 'attribute_type' => 'string', 'attribute_value' => 'https://www.facebook.com/'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'updated_at', 'attribute_type' => 'timestamp', 'attribute_value' => '2016-12-07T11:00:00+09:00'], // facebook -> update_time
                ['attribute_group' => 'user_info', 'attribute_name' => 'email', 'attribute_type' => 'string', 'attribute_value' => 'marcos@releasepad.com'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'email_verified', 'attribute_type' => 'boolean', 'attribute_value' => true], // facebook -> verified
                ['attribute_group' => 'facebook_user_info', 'attribute_name' => 'id', 'attribute_type' => 'string', 'attribute_value' => '123456'],
                ['attribute_group' => 'facebook_user_info', 'attribute_name' => 'cover', 'attribute_type' => 'string', 'attribute_value' => null],
                ['attribute_group' => 'facebook_user_info', 'attribute_name' => 'age_range', 'attribute_type' => 'string', 'attribute_value' => '40-50'],
                ['attribute_group' => 'facebook_user_info', 'attribute_name' => 'link', 'attribute_type' => 'string', 'attribute_value' => null],
                ['attribute_group' => 'facebook_user_info', 'attribute_name' => 'timezone', 'attribute_type' => 'string', 'attribute_value' => 'JST']
            ],
            'origin' => 'facebook',
            'created_at' => '2016-12-07T02:26:00+00:00'
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetAttributeSet() {
        // Get a access token for the next request
        $response = $this->mockRequest('POST', '/api/oauth2/token', [
            'grant_type' => 'password',
            'client_id' => 'server',
            'username' => 'test@test.com',
            'password' => 'test'
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $response_body = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('access_token', $response_body);
        $this->accessToken = $response_body['access_token'];
        // Get the latest user_info data
        $response = $this->mockRequest('GET', '/api/users/attribute_sets/query', [
            'attribute_groups' => ['user_info'],
            'attribute_names' => [],
            'method' => 'latest'
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $parsed_response = json_decode($response->getBody(), true);
        $this->assertEquals([
            'sets' => [[
                ['attribute_group' => 'user_info', 'attribute_name' => 'email', 'attribute_type' => 'string', 'attribute_value' => 'marcos@releasepad.com', 'created_at' => $parsed_response['sets'][0][0]['created_at'], 'origin' => 'facebook'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'email_verified', 'attribute_type' => 'boolean', 'attribute_value' => true, 'created_at' => $parsed_response['sets'][0][1]['created_at'], 'origin' => 'facebook'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'family_name', 'attribute_type' => 'string', 'attribute_value' => 'Cooper', 'created_at' => $parsed_response['sets'][0][2]['created_at'], 'origin' => 'facebook'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'gender', 'attribute_type' => 'string', 'attribute_value' => 'Male', 'created_at' => $parsed_response['sets'][0][3]['created_at'], 'origin' => 'facebook'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'given_name', 'attribute_type' => 'string', 'attribute_value' => 'Marcos', 'created_at' => $parsed_response['sets'][0][4]['created_at'], 'origin' => 'facebook'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'locale', 'attribute_type' => 'string', 'attribute_value' => 'ja_JP', 'created_at' => $parsed_response['sets'][0][5]['created_at'], 'origin' => 'facebook'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'name', 'attribute_type' => 'string', 'attribute_value' => 'Marcos Cooper', 'created_at' => $parsed_response['sets'][0][6]['created_at'], 'origin' => 'facebook'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'picture', 'attribute_type' => 'string', 'attribute_value' => 'https://www.facebook.com/', 'created_at' => $parsed_response['sets'][0][7]['created_at'], 'origin' => 'facebook'],
                ['attribute_group' => 'user_info', 'attribute_name' => 'updated_at', 'attribute_type' => 'timestamp', 'attribute_value' => $parsed_response['sets'][0][8]['attribute_value'], 'created_at' => $parsed_response['sets'][0][8]['created_at'], 'origin' => 'facebook']
            ]]
        ], $parsed_response);
        // Get the aggregated raw weather data from facebook for today
        /* $response = $this->mockRequest('GET', '/api/users/attribute/set', [
            'attribute_groups' => ['weather'],
            'attribute_names' => [],
            'method' => 'raw',
            'options' => [
                'from' => '2016-12-07T00:00:00+09:00',
                'to' => '2016-12-07T23:59:59+09:00',
                'only_sets_with' => [
                    'origins' => ['facebook']
                ]
            ]
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals([
            'sets' => [[
                ['attribute_group' => 'weather', 'attribute_name' => 'weather', 'attribute_type' => 'string', 'attribute_value' => 'cloudy'],
                ['attribute_group' => 'weather', 'attribute_name' => 'temperature', 'attribute_type' => 'double', 'attribute_value' => 12.0],
                ['attribute_group' => 'weather', 'attribute_name' => 'max_temperature', 'attribute_type' => 'double', 'attribute_value' => 12.0],
                ['attribute_group' => 'weather', 'attribute_name' => 'min_temperature', 'attribute_type' => 'double', 'attribute_value' => 6.0],
                ['attribute_group' => 'weather', 'attribute_name' => 'humidity', 'attribute_type' => 'double', 'attribute_value' => 70],
                ['attribute_group' => 'weather', 'attribute_name' => 'max_humidity', 'attribute_type' => 'double', 'attribute_value' => 75],
                ['attribute_group' => 'weather', 'attribute_name' => 'min_humidity', 'attribute_type' => 'double', 'attribute_value' => 65],
                ['attribute_group' => 'weather', 'attribute_name' => 'avg_humidity', 'attribute_type' => 'double', 'attribute_value' => 70],
                ['attribute_group' => 'weather', 'attribute_name' => 'rain_percentage', 'attribute_type' => 'double', 'attribute_value' => 35],
                ['attribute_group' => 'weather', 'attribute_name' => 'precipitation', 'attribute_type' => 'double', 'attribute_value' => 10],
                ['attribute_group' => 'weather', 'attribute_name' => 'sunrise', 'attribute_type' => 'timestamp', 'attribute_value' => '2016-12-07T06:48:00+09:00'],
                ['attribute_group' => 'weather', 'attribute_name' => 'sunset', 'attribute_type' => 'timestamp', 'attribute_value' => '2016-12-07T16:40:00+09:00'],
                ['attribute_group' => 'weather', 'attribute_name' => 'created_at', 'attribute_type' => 'timestamp', 'attribute_value' => '2016-12-07T11:00:00+09:00'],
            ]]
        ], $response); */
    }
}
