<?php
namespace Tests\Controllers;

use Slim\App;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AbstractControllerTestCase
 * @package Tests\Controllers
 */
abstract class ControllerTestCase extends \PHPUnit_Framework_TestCase {
    /**
     * @var App
     */
    protected $app;

    /**
     * @var string
     */
    protected $accessToken;

    /**
     * @param null $name
     * @param array $data
     * @param string $data_name
     */
    public function __construct($name = null, array $data = array(), $data_name = '') {
        parent::__construct($name, $data, $data_name);
        $this->app = $app = new App([
            'settings' => require __DIR__.'/../../settings.php'
        ]);
        require __DIR__.'/../../dependencies.php';
        require __DIR__.'/../../middleware.php';
        require __DIR__.'/../../routes.php';
    }

    /**
     * @param $request_method
     * @param $request_uri
     * @param null $request_data
     * @return Response
     */
    protected function mockRequest($request_method, $request_uri, $request_data = null) {
        $environment = Environment::mock([
            'HTTP_AUTHORIZATION' => $this->accessToken ? 'Bearer '.$this->accessToken : null,
            'REQUEST_METHOD' => $request_method,
            'REQUEST_URI' => $request_uri
        ]);
        $request = Request::createFromEnvironment($environment);
        if (isset($request_data)) {
            $request = $request->withParsedBody($request_data);
        }
        $response = new Response();
        $response = $this->app->process($request, $response);
        return $response;
    }
}
