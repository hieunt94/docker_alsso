<?php
namespace Tests\Controllers;

/**
 * Class RegistrationControllerTest
 * @package Tests\Controllers
 */
class MessageControllerTest extends ControllerTestCase {
    public function testPostLabelSet() {
        // Get a access token for the next request
        $response = $this->mockRequest('POST', '/api/oauth2/token', [
            'grant_type' => 'password',
            'client_id' => 'server',
            'username' => 'test@test.com',
            'password' => 'test'
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $response_body = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('access_token', $response_body);
        $this->accessToken = $response_body['access_token'];
        // Post a label set as a user
        $response = $this->mockRequest('POST', '/api/message/label/set', [
            'message_id' => 1,
            'labels' => [
                ['label_group' => 'post', 'label_name' => 'link', 'label_type' => 'string', 'label_value' => 'https://www.facebook.com/'],
                ['label_group' => 'post', 'label_name' => 'message', 'label_type' => 'string', 'label_value' => 'Hello there!'],
                ['label_group' => 'post', 'label_name' => 'created_at', 'label_type' => 'timestamp', 'label_value' => '2016-12-07T11:00:00+09:00'], // facebook -> created_time
                ['label_group' => 'facebook_post', 'label_name' => 'id', 'label_type' => 'string', 'label_value' => ''],
                ['label_group' => 'facebook_post', 'label_name' => 'place', 'label_type' => 'string', 'label_value' => null]
            ],
            'origin' => 'facebook',
            'created_at' => '2016-12-07T02:26:00+00:00'
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetAttributeSet() {
    }
}
