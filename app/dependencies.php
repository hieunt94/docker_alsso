<?php
use App\Services\OAuth2\AuthService;
use App\Services\OAuth2\SessionService;
use App\Services\OAuth2\StorageService;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use OAuth2\GrantType\ClientCredentials;
use OAuth2\GrantType\RefreshToken;
use OAuth2\GrantType\UserCredentials;
use OAuth2\OpenID\GrantType\AuthorizationCode;
use OAuth2\Server;

$container = $app->getContainer();
$settings = $container->get('settings');

// Vendor services

$container['logger'] = function () use ($settings) {
    $logger = new Logger($settings['logger']['name']);
    $logger->pushProcessor(new UidProcessor());
    $logger->pushHandler(new StreamHandler($settings['logger']['path'], Logger::DEBUG));
    return $logger;
};

$container['pdo'] = function () use ($settings) {
    return new PDO($settings['pdo']['dsn'], $settings['pdo']['username'], $settings['pdo']['password']);
};

$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(__DIR__.'/templates', [
        'cache' => __DIR__.'/../cache',
        'auto_reload' => true
    ]);
    $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($c['router'], $basePath));
    return $view;
};

$container['oauth2_server'] = function ($c) {
    $storage = new StorageService($c->get('pdo'));
    $server = new Server($storage, [
        'use_openid_connect' => true,
        'issuer' => isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'localhost'
    ]);
    $server->addGrantType(new UserCredentials($storage));
    $server->addGrantType(new ClientCredentials($storage));
    $server->addGrantType(new AuthorizationCode($storage));
    $server->addGrantType(new RefreshToken($storage, [
        'always_issue_new_refresh_token' => true
    ]));
    return $server;
};

$serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
$serviceContainer->checkVersion('2.0.0-dev');
$serviceContainer->setAdapterClass('default', 'mysql');
$serviceContainer->setLogger('defaultLogger', $container->get('logger'));
$manager = new \Propel\Runtime\Connection\ConnectionManagerSingle();
$manager->setConfiguration([
    'dsn' => $settings['pdo']['dsn'],
    'user' => $settings['pdo']['username'],
    'password' => $settings['pdo']['password'],
    'settings' => ['charset' => 'utf8', 'queries' => []],
    'classname' => '\\Propel\\Runtime\\Connection\\DebugPDO',
    'model_paths' => [0 => 'src', 1 => 'vendor']
]);
$manager->setName('default');
$serviceContainer->setConnectionManager('default', $manager);
$serviceContainer->setDefaultDatasource('default');

// App services

$container['auth_service'] = function ($c) {
    return new \App\Services\AuthService($c);
};

$container['mail_service'] = function ($c) {
    return new \App\Services\MailService($c);
};

$container['user_service'] = function ($c) {
    return new \App\Services\UserService($c);
};

$container['message_service'] = function ($c) {
    return new \App\Services\MessageService($c);
};