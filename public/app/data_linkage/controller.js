app.controller('data_linkageController', ['$scope', '$controller', '$http', '$window', 'authService', 'appSettings', 'supportService', 
    function ($scope, $controller, $http, $window, authService, appSettings, supportService) {

        $controller('navigationController', {$scope: $scope});
        $scope.isLoginSlack = false;
        $scope.isLoginFb = false;
        $scope.isLoginGoogle = false;
        $scope.isLoginTwitter = false;

        var accessToken = authService.access_token();
        var params = {'params': {alt_token: accessToken}};

        $scope.init = function() {
            var p = {headers: {'Authorization': 'Bearer ' + accessToken}};
            $http.post("/api/users/attribute_sets/query", {'attribute_groups': ['token'], 'attribute_names': ['access_token'], 'method': 'latest'}, p).then(function (response) {
                if (response.status == 200 && response.data.sets.length > 0) {
                    for (var i = 0; i < response.data.sets.length; i++) {
                        var sets = response.data.sets[i];

                        if (sets && sets.length > 0) {
                            for (var j = 0; j < sets.length; j++) {
                                var set = sets[j];

                                if (set.attribute_value) {
                                    if (set.origin == 'slack') {
                                        $scope.isLoginSlack = true;
                                    }
                                    
                                    if (set.origin == 'facebook') {
                                        $scope.isLoginFb = true;
                                    }

                                    if (set.origin == 'google') {
                                        $scope.isLoginGoogle = true;
                                    }

                                    if (set.origin == 'twitter') {
                                        $scope.isLoginTwitter = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }, supportService.errorHandler);
        };
        
        $scope.loginWithSlack = function() {
            if ($scope.isLoginSlack) {
                $http.get(appSettings.snsDomain + '/api/slack/get_login_uri', params).then(function (response) {
                    $window.location.href = response.data.login_url;
                });
            }
        };

        $scope.loginWithFb = function() {
            if ($scope.isLoginFb) {
                $http.get(appSettings.snsDomain + '/api/facebook/get_login_uri', params).then(function (response) {
                    $window.location.href = response.data.login_url;
                });
            }
        };

        $scope.loginWithTwitter = function() {
            if ($scope.isLoginTwitter) {
                $http.get(appSettings.snsDomain + '/api/twitter/get_login_uri', params).then(function (response) {
                    $window.location.href = response.data.login_url;
                });
            }
        };

        $scope.loginWithGoogle = function() {
            if ($scope.isLoginGoogle) {
                $http.get(appSettings.snsDomain + '/api/google/get_login_uri', params).then(function (response) {
                    $window.location.href = response.data.login_url;
                });
            }
        };
        
        $scope.init();
    }
]);