app.controller('authorizeController', ['$scope', '$location', 'authService',
    function ($scope, $location, authService) {
        $scope.access_token = authService.access_token();
        $scope.action = $location.url();
    }
]);