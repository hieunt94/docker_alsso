app.controller('itemsController', ['$scope', '$http', '$controller', 'notify', 'authService', 'supportService', 'Upload',
    function ($scope, $http, $controller, notify, authService, supportService, Upload) {

        $controller('navigationController', {$scope: $scope});
        var accessToken = authService.access_token();

        $scope.init = function(){
            $http.get('/api/oauth2/user_info', {
                headers: {'Authorization': 'Bearer ' + accessToken}
            }).then(function (response) {
                $scope.user = response.data;
                if ($scope.user.birth_date !== null) {
                    $scope.user.birth_date = new Date($scope.user.birth_date);
                }
            });
        }

        $scope.install = function () {

        }
        
        $scope.open = function () {

        }
        
        $scope.init();
    }
]);