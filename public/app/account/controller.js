app.controller('accountController', ['$scope', '$http', '$controller', 'notify', 'authService', 'supportService', 'Upload',
    function ($scope, $http, $controller, notify, authService, supportService, Upload) {

        $controller('navigationController', {$scope: $scope});
        var accessToken = authService.access_token();

        $scope.init = function(){
            $http.get('/api/oauth2/user_info', {
                headers: {'Authorization': 'Bearer ' + accessToken}
            }).then(function (response) {
                $scope.user = response.data;
                if ($scope.user.birth_date !== null) {
                    $scope.user.birth_date = new Date($scope.user.birth_date);
                }
            });
        }

        $scope.edit = function () {
            if ($scope.user.new_password == $scope.confirm_password) {
                $http.post('/api/users/update', $scope.user, {
                    headers: {'Authorization': 'Bearer ' + accessToken}
                }).then(function (response) {
                    notify({
                        message: "Your profile information has been changed.",
                        position: 'right',
                    });
                }, supportService.errorHandler);
            } else {
                notify({
                    message: "New password not matching confirm password",
                    position: 'right',
                    classes: 'alert-error'
                });
            }
        }
        
        $scope.init();
    }
]);