var app = angular.module('sso', [
    'ngRoute', 'ngFileUpload', 'frapontillo.bootstrap-switch', 'cgNotify'
]).config(function ($httpProvider) {
    // Defaults for the $http service
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=UTF-8';
}).config(['$locationProvider', function ($locationProvider) {
    $locationProvider.html5Mode(true);
}]);

app.config(function ($routeProvider) {
    $routeProvider.when("/api/oauth2/authorize", {
        controller: "authorizeController",
        templateUrl: "app/authorize/index.html?d=" + ~~(new Date / 1000)
    });
    angular.forEach(['error', 'login', 'signup', 'signup_verification', 'signup_completed', 'terms_of_use', 'items', 'account', 'avatar', 'data_linkage', 'profile', 'friends', 'debug'], function (val) {
        $routeProvider.when("/" + val, {
            controller: val + "Controller",
            templateUrl: "app/" + val + "/index.html?d=" + ~~(new Date / 1000)
        });
    });
});

app.constant('appSettings', {
    isDev: false,
    host: 'http://local.sso:8080/',
    snsDomain: 'http://sns-dev.alt.ai'
});

app.controller('navigationController', ['$scope', '$location', 'authService',
    function ($scope, $location, authService) {
        $scope.$watch(function () {
            return authService.isLogged();
        }, function (isLogged) {
            $scope.isLogged = isLogged;
        });
        $scope.logout = function () {
            authService.logout();
            $location.path('/login');
        };
        $scope.leftNavigation = function () {
            return 'app/templates/left_navigation.html?d=1';
        };
        $scope.isPage = function (include_str) {
            return $location.absUrl().indexOf(include_str) != -1;
        };
    }
]);

app.run(['$rootScope', '$location', '$route', 'authService',
    function ($rootScope, $location, $route, authService) {
        var init = authService.init();
        $rootScope.$on('$routeChangeStart', function (event, next) {
            init.then(function () {
                if (!authService.isLogged()) {
                    if ($.inArray(next['controller'], ['loginController', 'signupController', 'signup_verificationController', 'signup_completedController', 'terms_of_useController', 'errorController']) == -1) {
                        $location.path('/login');
                    }
                }
            });
        });
    }
]);