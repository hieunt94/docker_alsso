app.factory('userService', ['$http',
    function ($http) {
        return {
            signup: function (user) {
                return $http.post('/api/users/signup', user);
            },
            verify: function (uid, verification_key) {
                return $http.post('/api/users/verify/email?uid=' + uid + '&verification_key=' + verification_key, {
                    'uid': uid,
                    'verification_key': verification_key
                });
            }
        };
    }
]);