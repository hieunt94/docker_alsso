app.factory('authService', ['$q', '$http',
    function ($q, $http) {
        return {
            init: function () {
                var that = this;
                return $q(function (resolve) {
                    if (that.isLogged()) {
                        var refresh_token = localStorage.getItem('oauth2_refresh_token');
                        $http.post('/api/oauth2/token', {
                            'grant_type': 'refresh_token',
                            'client_id': 'server',
                            'refresh_token': refresh_token
                        }).then(function (response) {
                            localStorage.setItem('oauth2_access_token', response.data['access_token']);
                            localStorage.setItem('oauth2_refresh_token', response.data['refresh_token']);
                            localStorage.setItem('oauth2_expires_at', Date.now() + (response.data['expires_in'] * 1000));
                            resolve(true);
                        }, function () {
                            resolve(false);
                        });
                    } else {
                        resolve(false);
                    }
                });
            },
            isLogged: function () {
                var expires_at = localStorage.getItem('oauth2_expires_at');
                if (expires_at) {
                    return expires_at > Date.now();
                }
                return false;
            },
            login: function (username, password) {
                return $http.post('/api/oauth2/token', {
                    'grant_type': 'password',
                    'client_id': 'server',
                    'username': username,
                    'password': password
                }).then(function (response) {
                    localStorage.setItem('oauth2_access_token', response.data['access_token']);
                    localStorage.setItem('oauth2_refresh_token', response.data['refresh_token']);
                    localStorage.setItem('oauth2_expires_at', Date.now() + (response.data['expires_in'] * 1000));
                });
            },
            logout: function () {
                localStorage.removeItem('oauth2_access_token');
                localStorage.removeItem('oauth2_refresh_token');
                localStorage.removeItem('oauth2_expires_at');
            },
            access_token: function () {
                return localStorage.getItem('oauth2_access_token');
            }
        };
    }
]);