app.factory('supportService', ['notify',
    function (notify) {
        return {
            user: null,
            errorHandler: function (response) {
                if (response.data['error_description']) {
                    notify({
                        message: response.data['error_description'],
                        position: 'center',
                        classes: 'alert-error'
                    });
                } else {
                    notify({
                        message: 'Fatal error',
                        position: 'center',
                        classes: 'alert-error'
                    });
                }
            }
        };
    }
]);