app.controller('signupController', ['$scope', '$location', 'userService', 'supportService', 'notify',
    function ($scope, $location, userService, supportService, notify) {
        $scope.user = {};

        $scope.signup = function () {
            if ($scope.user.given_name && $scope.user.family_name && $scope.user.gender && $scope.user.email && $scope.user.password) {
                userService.signup($scope.user).then(function (response) {
                    supportService.user = response.data.data;
                    $location.path('/signup_verification');
                }, supportService.errorHandler);
            } else {
                notify({
                    message: 'Required fields are missing',
                    position: 'center',
                    classes: 'alert-error'
                });
            }
        }
    }
]);