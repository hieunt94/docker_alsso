app.controller('loginController', ['$scope', '$location', '$routeParams', 'authService', 'supportService', 'notify',
    function ($scope, $location, $routeParams, authService, supportService, notify) {
        $scope.username = '';

        $scope.password = '';

        $scope.login = function () {
            if ($scope.username && $scope.password) {
                authService.login($scope.username, $scope.password).then(function () {
                    if ($routeParams['response_type'] == 'code') {
                        $location.path('/api/oauth2/authorize');
                    } else {
                        $location.path('/account');
                    }
                }, supportService.errorHandler);
            } else {
                notify({
                    message: 'Required fields are missing',
                    position: 'center',
                    classes: 'alert-danger'
                });
            }
        }
    }
]);