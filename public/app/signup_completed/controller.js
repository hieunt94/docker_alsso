app.controller('signup_completedController', ['$scope', '$location', '$routeParams', 'userService',
    function ($scope, $location, $routeParams, userService) {
        $scope.loginIsEnabled = false;

        userService.verify($routeParams['uid'], $routeParams['verification_key']).then(function () {
            $scope.loginIsEnabled = true;
        }, function () {
            $location.path('/error');
        });
    }
]);