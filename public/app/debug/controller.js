app.controller('debugController', ['$scope', '$http', 'authService', 'supportService',
    function ($scope, $http, authService, supportService) {

        var accessToken = authService.access_token();

        $http.get('/api/users/attribute_names', {
            headers: {'Authorization': 'Bearer ' + accessToken}
        }).then(function (response) {
            $scope.attribute_groups = response.data;
        });

        $scope.methods = ['latest'];

        $scope.request = {
            'attribute_groups': [],
            'attribute_names': [],
            'method': 'latest'
        };

        $scope.sets = {};

        $scope.selectAll = function (group) {
            $('.' + group + ' input').attr('checked', true);
            $('.' + group + ' input').prop('checked', true);
            $('.' + group + ' input').each(function () {
                var idx = $scope.request.attribute_names.indexOf($(this).val());
                if (idx > -1) {
                } else {
                    $scope.request.attribute_names.push($(this).val());
                }
            });
        };

        $scope.selectNone = function (group) {
            $('.' + group + ' input').attr('checked', false);
            $('.' + group + ' input').prop('checked', false);
            $('.' + group + ' input').each(function () {
                var idx = $scope.request.attribute_names.indexOf($(this).val());
                if (idx > -1) {
                    $scope.request.attribute_names.splice(idx, 1);
                }
            });
        };

        $scope.toggleAttribute = function ($event, attribute) {
            var idx = $scope.request.attribute_names.indexOf(attribute);
            if (idx > -1) {
                $scope.request.attribute_names.splice(idx, 1);
            } else {
                $scope.request.attribute_names.push(attribute);
            }
        };

        $scope.submit = function () {
            $http.post('/api/users/attribute_sets/query', $scope.request, {
                headers: {'Authorization': 'Bearer ' + accessToken},
            }).then(function (response) {
                $scope.sets = response.data;
            }, supportService.errorHandler);
        }
    }
]);