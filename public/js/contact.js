var $ = jQuery.noConflict();
jQuery(function($) {
	"use strict";
	
	// CONTACT FORM
	$("#contactform").each(function() {
		$('#contactform').validate({
			highlight: function(element, errorClass) {
				$(element).fadeOut(function() {
					$(element).fadeIn();
				});
			},
			rules: {
				name: {
					required: true,
					minlength: 2
				},
				email: {
					required: true,
					email: true
				},
				//number: {
//					required: false,
//					minlength: 8
//				},
//				contents: {
//					required: false
//				},
				message: {
					required: true,
					minlength: 5
				}
			},
			messages: {
				name: "<i class='fa fa-exclamation-triangle'></i> Please specify your name",
				email: {
					required: "<i class='fa fa-exclamation-triangle'></i> We need your email address to contact you",
					email: "<i class='fa fa-exclamation-triangle'></i> Please enter a valid email address"
				},
				number: "<i class='fa fa-exclamation-triangle'></i> Please specify your phone",
				contents: "<i class='fa fa-exclamation-triangle'></i> Please select your contents",
				message: "<i class='fa fa-exclamation-triangle'></i> Please enter your message"
			},
			submitHandler: function(form) {
				$(form).ajaxSubmit({
					type: "POST",
					data: $(form).serialize(),
					url: "inc/contact.php",
					success: function() {
						$('#contactform :input').attr('disabled', 'disabled');
						$('#contactform').fadeTo("slow", 0.15, function() {
							$(this).find(':input').attr('disabled', 'disabled');
							$(this).find('label').css('cursor', 'default');
							$(".contactform").delay(1000).slideUp("slow");
							$(".contact .message").delay(1500).slideDown("slow");
							$('.success').delay(1500).slideDown("slow");
						});
					},
					error: function() {
						$('#contactform').fadeTo("slow", 0.15, function() {
							$(".contactform").delay(1000).slideUp("slow");
							$(".contact .message").delay(1500).slideDown("slow");
							$('.error').delay(1500).slideDown("slow");
						});
					}
				});
			}
		});
	});	
	
});