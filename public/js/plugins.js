/*
* www.Pooyaa.com
* Copyright (c) 2014-2015 Pooyaa;
*/

/*! FORM */
(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], factory)
    } else {
        factory((typeof(jQuery) != "undefined")
            ? jQuery
            : window.Zepto)
    }
}(function ($) {
    var feature = {};
    feature.fileapi = $("<input type='file'/>").get(0).files !== undefined;
    feature.formdata = window.FormData !== undefined;
    var hasProp = !!$.fn.prop;
    $.fn.attr2 = function () {
        if (!hasProp) {
            return this.attr.apply(this, arguments)
        }
        var val = this.prop.apply(this, arguments);
        if ((val && val.jquery) || typeof val === "string") {
            return val
        }
        return this.attr.apply(this, arguments)
    };
    $.fn.ajaxSubmit = function (options) {
        if (!this.length) {
            log("ajaxSubmit: skipping submit process - no element selected");
            return this
        }
        var method,
            action,
            url,
            $form = this;
        if (typeof options == "function") {
            options = {
                success: options
            }
        } else {
            if (options === undefined) {
                options = {}
            }
        }
        method = options.type || this.attr2("method");
        action = options.url || this.attr2("action");
        url = (typeof action === "string")
            ? $.trim(action)
            : "";
        url = url || window.location.href || "";
        if (url) {
            url = (url.match(/^([^#]+)/) || [])[1]
        }
        options = $.extend(true, {
            url: url,
            success: $.ajaxSettings.success,
            type: method || $.ajaxSettings.type,
            iframeSrc: /^https/i.test(window.location.href || "")
                ? "javascript:false": "about:blank"
        }, options);
        var veto = {};
        this.trigger("form-pre-serialize", [
            this, options, veto
        ]);
        if (veto.veto) {
            log("ajaxSubmit: submit vetoed via form-pre-serialize trigger");
            return this
        }
        if (options.beforeSerialize && options.beforeSerialize(this, options) === false) {
            log("ajaxSubmit: submit aborted via beforeSerialize callback");
            return this
        }
        var traditional = options.traditional;
        if (traditional === undefined) {
            traditional = $.ajaxSettings.traditional
        }
        var elements = [];
        var qx,
            a = this.formToArray(options.semantic, elements);
        if (options.data) {
            options.extraData = options.data;
            qx = $.param(options.data, traditional)
        }
        if (options.beforeSubmit && options.beforeSubmit(a, this, options) === false) {
            log("ajaxSubmit: submit aborted via beforeSubmit callback");
            return this
        }
        this.trigger("form-submit-validate", [
            a, this, options, veto
        ]);
        if (veto.veto) {
            log("ajaxSubmit: submit vetoed via form-submit-validate trigger");
            return this
        }
        var q = $.param(a, traditional);
        if (qx) {
            q = (q
                ? (q + "&" + qx)
                : qx)
        }
        if (options.type.toUpperCase() == "GET") {
            options.url += (options.url.indexOf("?") >= 0
                ? "&"
                : "?") + q;
            options.data = null
        } else {
            options.data = q
        }
        var callbacks = [];
        if (options.resetForm) {
            callbacks.push(function () {
                $form.resetForm()
            })
        }
        if (options.clearForm) {
            callbacks.push(function () {
                $form.clearForm(options.includeHidden)
            })
        }
        if (!options.dataType && options.target) {
            var oldSuccess = options.success || function () {};
            callbacks.push(function (data) {
                var fn = options.replaceTarget
                    ? "replaceWith"
                    : "html";
                $(options.target)[fn](data).each(oldSuccess, arguments)
            })
        } else {
            if (options.success) {
                callbacks.push(options.success)
            }
        }
        options.success = function (data, status, xhr) {
            var context = options.context || this;
            for (var i = 0, max = callbacks.length; i < max; i++) {
                callbacks[i].apply(context, [
                    data, status, xhr || $form, $form
                ])
            }
        };
        if (options.error) {
            var oldError = options.error;
            options.error = function (xhr, status, error) {
                var context = options.context || this;
                oldError.apply(context, [
                    xhr, status, error, $form
                ])
            }
        }
        if (options.complete) {
            var oldComplete = options.complete;
            options.complete = function (xhr, status) {
                var context = options.context || this;
                oldComplete.apply(context, [
                    xhr, status, $form
                ])
            }
        }
        var fileInputs = $("input[type=file]:enabled", this).filter(function () {
            return $(this).val() !== ""
        });
        var hasFileInputs = fileInputs.length > 0;
        var mp = "multipart/form-data";
        var multipart = ($form.attr("enctype") == mp || $form.attr("encoding") == mp);
        var fileAPI = feature.fileapi && feature.formdata;
        log("fileAPI :" + fileAPI);
        var shouldUseFrame = (hasFileInputs || multipart) && !fileAPI;
        var jqxhr;
        if (options.iframe !== false && (options.iframe || shouldUseFrame)) {
            if (options.closeKeepAlive) {
                $.get(options.closeKeepAlive, function () {
                    jqxhr = fileUploadIframe(a)
                })
            } else {
                jqxhr = fileUploadIframe(a)
            }
        } else {
            if ((hasFileInputs || multipart) && fileAPI) {
                jqxhr = fileUploadXhr(a)
            } else {
                jqxhr = $.ajax(options)
            }
        }
        $form.removeData("jqxhr").data("jqxhr", jqxhr);
        for (var k = 0; k < elements.length; k++) {
            elements[k] = null
        }
        this.trigger("form-submit-notify", [
            this, options
        ]);
        return this;
        function deepSerialize(extraData) {
            var serialized = $.param(extraData, options.traditional).split("&");
            var len = serialized.length;
            var result = [];
            var i,
                part;
            for (i = 0; i < len; i++) {
                serialized[i] = serialized[i].replace(/\+/g, " ");
                part = serialized[i].split("=");
                result.push([decodeURIComponent(part[0]), decodeURIComponent(part[1])])
            }
            return result
        }
        function fileUploadXhr(a) {
            var formdata = new FormData();
            for (var i = 0; i < a.length; i++) {
                formdata.append(a[i].name, a[i].value)
            }
            if (options.extraData) {
                var serializedData = deepSerialize(options.extraData);
                for (i = 0; i < serializedData.length; i++) {
                    if (serializedData[i]) {
                        formdata.append(serializedData[i][0], serializedData[i][1])
                    }
                }
            }
            options.data = null;
            var s = $.extend(true, {}, $.ajaxSettings, options, {
                contentType: false,
                processData: false,
                cache: false,
                type: method || "POST"
            });
            if (options.uploadProgress) {
                s.xhr = function () {
                    var xhr = $.ajaxSettings.xhr();
                    if (xhr.upload) {
                        xhr.upload.addEventListener("progress", function (event) {
                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total;
                            if (event.lengthComputable) {
                                percent = Math.ceil(position / total * 100)
                            }
                            options.uploadProgress(event, position, total, percent)
                        }, false)
                    }
                    return xhr
                }
            }
            s.data = null;
            var beforeSend = s.beforeSend;
            s.beforeSend = function (xhr, o) {
                if (options.formData) {
                    o.data = options.formData
                } else {
                    o.data = formdata
                }
                if (beforeSend) {
                    beforeSend.call(this, xhr, o)
                }
            };
            return $.ajax(s)
        }
        function fileUploadIframe(a) {
            var form = $form[0],
                el,
                i,
                s,
                g,
                id,
                $io,
                io,
                xhr,
                sub,
                n,
                timedOut,
                timeoutHandle;
            var deferred = $.Deferred();
            deferred.abort = function (status) {
                xhr.abort(status)
            };
            if (a) {
                for (i = 0; i < elements.length; i++) {
                    el = $(elements[i]);
                    if (hasProp) {
                        el.prop("disabled", false)
                    } else {
                        el.removeAttr("disabled")
                    }
                }
            }
            s = $.extend(true, {}, $.ajaxSettings, options);
            s.context = s.context || s;
            id = "jqFormIO" + (new Date().getTime());
            if (s.iframeTarget) {
                $io = $(s.iframeTarget);
                n = $io.attr2("name");
                if (!n) {
                    $io.attr2("name", id)
                } else {
                    id = n
                }
            } else {
                $io = $('<iframe name="' + id + '" src="' + s.iframeSrc + '" />');
                $io.css({
                    position: "absolute",
                    top: "-1000px",
                    left: "-1000px"
                })
            }
            io = $io[0];
            xhr = {
                aborted: 0,
                responseText: null,
                responseXML: null,
                status: 0,
                statusText: "n/a",
                getAllResponseHeaders: function () {},
                getResponseHeader: function () {},
                setRequestHeader: function () {},
                abort: function (status) {
                    var e = (status === "timeout"
                        ? "timeout"
                        : "aborted");
                    log("aborting upload... " + e);
                    this.aborted = 1;
                    try {
                        if (io.contentWindow.document.execCommand) {
                            io.contentWindow.document.execCommand("Stop")
                        }
                    } catch (ignore) {}
                    $io.attr("src", s.iframeSrc);
                    xhr.error = e;
                    if (s.error) {
                        s.error.call(s.context, xhr, e, status)
                    }
                    if (g) {
                        $.event.trigger("ajaxError", [
                            xhr, s, e
                        ])
                    }
                    if (s.complete) {
                        s.complete.call(s.context, xhr, e)
                    }
                }
            };
            g = s.global;
            if (g && 0 === $.active++) {
                $.event.trigger("ajaxStart")
            }
            if (g) {
                $.event.trigger("ajaxSend", [
                    xhr, s
                ])
            }
            if (s.beforeSend && s.beforeSend.call(s.context, xhr, s) === false) {
                if (s.global) {
                    $.active--
                }
                deferred.reject();
                return deferred
            }
            if (xhr.aborted) {
                deferred.reject();
                return deferred
            }
            sub = form.clk;
            if (sub) {
                n = sub.name;
                if (n && !sub.disabled) {
                    s.extraData = s.extraData || {};
                    s.extraData[n] = sub.value;
                    if (sub.type == "image") {
                        s.extraData[n + ".x"] = form.clk_x;
                        s.extraData[n + ".y"] = form.clk_y
                    }
                }
            }
            var CLIENT_TIMEOUT_ABORT = 1;
            var SERVER_ABORT = 2;
            function getDoc(frame) {
                var doc = null;
                try {
                    if (frame.contentWindow) {
                        doc = frame.contentWindow.document
                    }
                } catch (err) {
                    log("cannot get iframe.contentWindow document: " + err)
                }
                if (doc) {
                    return doc
                }
                try {
                    doc = frame.contentDocument
                        ? frame.contentDocument
                        : frame.document
                } catch (err) {
                    log("cannot get iframe.contentDocument: " + err);
                    doc = frame.document
                }
                return doc
            }
            var csrf_token = $("meta[name=csrf-token]").attr("content");
            var csrf_param = $("meta[name=csrf-param]").attr("content");
            if (csrf_param && csrf_token) {
                s.extraData = s.extraData || {};
                s.extraData[csrf_param] = csrf_token
            }
            function doSubmit() {
                var t = $form.attr2("target"),
                    a = $form.attr2("action"),
                    mp = "multipart/form-data",
                    et = $form.attr("enctype") || $form.attr("encoding") || mp;
                form.setAttribute("target", id);
                if (!method || /post/i.test(method)) {
                    form.setAttribute("method", "POST")
                }
                if (a != s.url) {
                    form.setAttribute("action", s.url)
                }
                if (!s.skipEncodingOverride && (!method || /post/i.test(method))) {
                    $form.attr({
                        encoding: "multipart/form-data",
                        enctype: "multipart/form-data"
                    })
                }
                if (s.timeout) {
                    timeoutHandle = setTimeout(function () {
                        timedOut = true;
                        cb(CLIENT_TIMEOUT_ABORT)
                    }, s.timeout)
                }
                function checkState() {
                    try {
                        var state = getDoc(io).readyState;
                        log("state = " + state);
                        if (state && state.toLowerCase() == "uninitialized") {
                            setTimeout(checkState, 50)
                        }
                    } catch (e) {
                        log("Server abort: ", e, " (", e.name, ")");
                        cb(SERVER_ABORT);
                        if (timeoutHandle) {
                            clearTimeout(timeoutHandle)
                        }
                        timeoutHandle = undefined
                    }
                }
                var extraInputs = [];
                try {
                    if (s.extraData) {
                        for (var n in s.extraData) {
                            if (s.extraData.hasOwnProperty(n)) {
                                if ($.isPlainObject(s.extraData[n]) && s.extraData[n].hasOwnProperty("name") && s.extraData[n].hasOwnProperty("value")) {
                                    extraInputs.push($('<input type="hidden" name="' + s.extraData[n].name + '">').val(s.extraData[n].value).appendTo(form)[0])
                                } else {
                                    extraInputs.push($('<input type="hidden" name="' + n + '">').val(s.extraData[n]).appendTo(form)[0])
                                }
                            }
                        }
                    }
                    if (!s.iframeTarget) {
                        $io.appendTo("body")
                    }
                    if (io.attachEvent) {
                        io.attachEvent("onload", cb)
                    } else {
                        io.addEventListener("load", cb, false)
                    }
                    setTimeout(checkState, 15);
                    try {
                        form.submit()
                    } catch (err) {
                        var submitFn = document.createElement("form").submit;
                        submitFn.apply(form)
                    }
                } finally {
                    form.setAttribute("action", a);
                    form.setAttribute("enctype", et);
                    if (t) {
                        form.setAttribute("target", t)
                    } else {
                        $form.removeAttr("target")
                    }
                    $(extraInputs).remove()
                }
            }
            if (s.forceSync) {
                doSubmit()
            } else {
                setTimeout(doSubmit, 10)
            }
            var data,
                doc,
                domCheckCount = 50,
                callbackProcessed;
            function cb(e) {
                if (xhr.aborted || callbackProcessed) {
                    return
                }
                doc = getDoc(io);
                if (!doc) {
                    log("cannot access response document");
                    e = SERVER_ABORT
                }
                if (e === CLIENT_TIMEOUT_ABORT && xhr) {
                    xhr.abort("timeout");
                    deferred.reject(xhr, "timeout");
                    return
                } else {
                    if (e == SERVER_ABORT && xhr) {
                        xhr.abort("server abort");
                        deferred.reject(xhr, "error", "server abort");
                        return
                    }
                }
                if (!doc || doc.location.href == s.iframeSrc) {
                    if (!timedOut) {
                        return
                    }
                }
                if (io.detachEvent) {
                    io.detachEvent("onload", cb)
                } else {
                    io.removeEventListener("load", cb, false)
                }
                var status = "success",
                    errMsg;
                try {
                    if (timedOut) {
                        throw "timeout"
                    }
                    var isXml = s.dataType == "xml" || doc.XMLDocument || $.isXMLDoc(doc);
                    log("isXml=" + isXml);
                    if (!isXml && window.opera && (doc.body === null || !doc.body.innerHTML)) {
                        if (--domCheckCount) {
                            log("requeing onLoad callback, DOM not available");
                            setTimeout(cb, 250);
                            return
                        }
                    }
                    var docRoot = doc.body
                        ? doc.body
                        : doc.documentElement;
                    xhr.responseText = docRoot
                        ? docRoot.innerHTML
                        : null;
                    xhr.responseXML = doc.XMLDocument
                        ? doc.XMLDocument
                        : doc;
                    if (isXml) {
                        s.dataType = "xml"
                    }
                    xhr.getResponseHeader = function (header) {
                        var headers = {
                            "content-type": s.dataType
                        };
                        return headers[header.toLowerCase()]
                    };
                    if (docRoot) {
                        xhr.status = Number(docRoot.getAttribute("status")) || xhr.status;
                        xhr.statusText = docRoot.getAttribute("statusText") || xhr.statusText
                    }
                    var dt = (s.dataType || "").toLowerCase();
                    var scr = /(json|script|text)/.test(dt);
                    if (scr || s.textarea) {
                        var ta = doc.getElementsByTagName("textarea")[0];
                        if (ta) {
                            xhr.responseText = ta.value;
                            xhr.status = Number(ta.getAttribute("status")) || xhr.status;
                            xhr.statusText = ta.getAttribute("statusText") || xhr.statusText
                        } else {
                            if (scr) {
                                var pre = doc.getElementsByTagName("pre")[0];
                                var b = doc.getElementsByTagName("body")[0];
                                if (pre) {
                                    xhr.responseText = pre.textContent
                                        ? pre.textContent
                                        : pre.innerText
                                } else {
                                    if (b) {
                                        xhr.responseText = b.textContent
                                            ? b.textContent
                                            : b.innerText
                                    }
                                }
                            }
                        }
                    } else {
                        if (dt == "xml" && !xhr.responseXML && xhr.responseText) {
                            xhr.responseXML = toXml(xhr.responseText)
                        }
                    }
                    try {
                        data = httpData(xhr, dt, s)
                    } catch (err) {
                        status = "parsererror";
                        xhr.error = errMsg = (err || status)
                    }
                } catch (err) {
                    log("error caught: ", err);
                    status = "error";
                    xhr.error = errMsg = (err || status)
                }
                if (xhr.aborted) {
                    log("upload aborted");
                    status = null
                }
                if (xhr.status) {
                    status = (xhr.status >= 200 && xhr.status < 300 || xhr.status === 304)
                        ? "success"
                        : "error"
                }
                if (status === "success") {
                    if (s.success) {
                        s.success.call(s.context, data, "success", xhr)
                    }
                    deferred.resolve(xhr.responseText, "success", xhr);
                    if (g) {
                        $.event.trigger("ajaxSuccess", [
                            xhr, s
                        ])
                    }
                } else {
                    if (status) {
                        if (errMsg === undefined) {
                            errMsg = xhr.statusText
                        }
                        if (s.error) {
                            s.error.call(s.context, xhr, status, errMsg)
                        }
                        deferred.reject(xhr, "error", errMsg);
                        if (g) {
                            $.event.trigger("ajaxError", [
                                xhr, s, errMsg
                            ])
                        }
                    }
                }
                if (g) {
                    $.event.trigger("ajaxComplete", [
                        xhr, s
                    ])
                }
                if (g && !--$.active) {
                    $.event.trigger("ajaxStop")
                }
                if (s.complete) {
                    s.complete.call(s.context, xhr, status)
                }
                callbackProcessed = true;
                if (s.timeout) {
                    clearTimeout(timeoutHandle)
                }
                setTimeout(function () {
                    if (!s.iframeTarget) {
                        $io.remove()
                    } else {
                        $io.attr("src", s.iframeSrc)
                    }
                    xhr.responseXML = null
                }, 100)
            }
            var toXml = $.parseXML || function (s, doc) {
                if (window.ActiveXObject) {
                    doc = new ActiveXObject("Microsoft.XMLDOM");
                    doc.async = "false";
                    doc.loadXML(s)
                } else {
                    doc = (new DOMParser()).parseFromString(s, "text/xml")
                }
                return (doc && doc.documentElement && doc.documentElement.nodeName != "parsererror")
                    ? doc
                    : null
            };
            var parseJSON = $.parseJSON || function (s) {
                return window["eval"]("(" + s + ")")
            };
            var httpData = function (xhr, type, s) {
                var ct = xhr.getResponseHeader("content-type") || "",
                    xml = type === "xml" || !type && ct.indexOf("xml") >= 0,
                    data = xml
                        ? xhr.responseXML
                        : xhr.responseText;
                if (xml && data.documentElement.nodeName === "parsererror") {
                    if ($.error) {
                        $.error("parsererror")
                    }
                }
                if (s && s.dataFilter) {
                    data = s.dataFilter(data, type)
                }
                if (typeof data === "string") {
                    if (type === "json" || !type && ct.indexOf("json") >= 0) {
                        data = parseJSON(data)
                    } else {
                        if (type === "script" || !type && ct.indexOf("javascript") >= 0) {
                            $.globalEval(data)
                        }
                    }
                }
                return data
            };
            return deferred
        }
    };
    $.fn.ajaxForm = function (options) {
        options = options || {};
        options.delegation = options.delegation && $.isFunction($.fn.on);
        if (!options.delegation && this.length === 0) {
            var o = {
                s: this.selector,
                c: this.context
            };
            if (!$.isReady && o.s) {
                log("DOM not ready, queuing ajaxForm");
                $(function () {
                    $(o.s, o.c).ajaxForm(options)
                });
                return this
            }
            log("terminating; zero elements found by selector" + ($.isReady
                ? ""
                : " (DOM not ready)"));
            return this
        }
        if (options.delegation) {
            $(document).off("submit.form-plugin", this.selector, doAjaxSubmit).off("click.form-plugin", this.selector, captureSubmittingElement).on("submit.form-plugin", this.selector, options, doAjaxSubmit).on("click.form-plugin", this.selector, options, captureSubmittingElement);
            return this
        }
        return this.ajaxFormUnbind().bind("submit.form-plugin", options, doAjaxSubmit).bind("click.form-plugin", options, captureSubmittingElement)
    };
    function doAjaxSubmit(e) {
        var options = e.data;
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            $(e.target).ajaxSubmit(options)
        }
    }
    function captureSubmittingElement(e) {
        var target = e.target;
        var $el = $(target);
        if (!($el.is("[type=submit],[type=image]"))) {
            var t = $el.closest("[type=submit]");
            if (t.length === 0) {
                return
            }
            target = t[0]
        }
        var form = this;
        form.clk = target;
        if (target.type == "image") {
            if (e.offsetX !== undefined) {
                form.clk_x = e.offsetX;
                form.clk_y = e.offsetY
            } else {
                if (typeof $.fn.offset == "function") {
                    var offset = $el.offset();
                    form.clk_x = e.pageX - offset.left;
                    form.clk_y = e.pageY - offset.top
                } else {
                    form.clk_x = e.pageX - target.offsetLeft;
                    form.clk_y = e.pageY - target.offsetTop
                }
            }
        }
        setTimeout(function () {
            form.clk = form.clk_x = form.clk_y = null
        }, 100)
    }
    $.fn.ajaxFormUnbind = function () {
        return this.unbind("submit.form-plugin click.form-plugin")
    };
    $.fn.formToArray = function (semantic, elements) {
        var a = [];
        if (this.length === 0) {
            return a
        }
        var form = this[0];
        var formId = this.attr("id");
        var els = semantic
            ? form.getElementsByTagName("*")
            : form.elements;
        var els2;
        if (els && !/MSIE [678]/.test(navigator.userAgent)) {
            els = $(els).get()
        }
        if (formId) {
            els2 = $(":input[form=" + formId + "]").get();
            if (els2.length) {
                els = (els || []).concat(els2)
            }
        }
        if (!els || !els.length) {
            return a
        }
        var i,
            j,
            n,
            v,
            el,
            max,
            jmax;
        for (i = 0, max = els.length; i < max; i++) {
            el = els[i];
            n = el.name;
            if (!n || el.disabled) {
                continue
            }
            if (semantic && form.clk && el.type == "image") {
                if (form.clk == el) {
                    a.push({
                        name: n,
                        value: $(el).val(),
                        type: el.type
                    });
                    a.push({
                        name: n + ".x",
                        value: form.clk_x
                    }, {
                        name: n + ".y",
                        value: form.clk_y
                    })
                }
                continue
            }
            v = $.fieldValue(el, true);
            if (v && v.constructor == Array) {
                if (elements) {
                    elements.push(el)
                }
                for (j = 0, jmax = v.length; j < jmax; j++) {
                    a.push({
                        name: n,
                        value: v[j]
                    })
                }
            } else {
                if (feature.fileapi && el.type == "file") {
                    if (elements) {
                        elements.push(el)
                    }
                    var files = el.files;
                    if (files.length) {
                        for (j = 0; j < files.length; j++) {
                            a.push({
                                name: n,
                                value: files[j],
                                type: el.type
                            })
                        }
                    } else {
                        a.push({
                            name: n,
                            value: "",
                            type: el.type
                        })
                    }
                } else {
                    if (v !== null && typeof v != "undefined") {
                        if (elements) {
                            elements.push(el)
                        }
                        a.push({
                            name: n,
                            value: v,
                            type: el.type,
                            required: el.required
                        })
                    }
                }
            }
        }
        if (!semantic && form.clk) {
            var $input = $(form.clk),
                input = $input[0];
            n = input.name;
            if (n && !input.disabled && input.type == "image") {
                a.push({
                    name: n,
                    value: $input.val()
                });
                a.push({
                    name: n + ".x",
                    value: form.clk_x
                }, {
                    name: n + ".y",
                    value: form.clk_y
                })
            }
        }
        return a
    };
    $.fn.formSerialize = function (semantic) {
        return $.param(this.formToArray(semantic))
    };
    $.fn.fieldSerialize = function (successful) {
        var a = [];
        this.each(function () {
            var n = this.name;
            if (!n) {
                return
            }
            var v = $.fieldValue(this, successful);
            if (v && v.constructor == Array) {
                for (var i = 0, max = v.length; i < max; i++) {
                    a.push({
                        name: n,
                        value: v[i]
                    })
                }
            } else {
                if (v !== null && typeof v != "undefined") {
                    a.push({
                        name: this.name,
                        value: v
                    })
                }
            }
        });
        return $.param(a)
    };
    $.fn.fieldValue = function (successful) {
        for (var val = [], i = 0, max = this.length; i < max; i++) {
            var el = this[i];
            var v = $.fieldValue(el, successful);
            if (v === null || typeof v == "undefined" || (v.constructor == Array && !v.length)) {
                continue
            }
            if (v.constructor == Array) {
                $.merge(val, v)
            } else {
                val.push(v)
            }
        }
        return val
    };
    $.fieldValue = function (el, successful) {
        var n = el.name,
            t = el.type,
            tag = el.tagName.toLowerCase();
        if (successful === undefined) {
            successful = true
        }
        if (successful && (!n || el.disabled || t == "reset" || t == "button" || (t == "checkbox" || t == "radio") && !el.checked || (t == "submit" || t == "image") && el.form && el.form.clk != el || tag == "select" && el.selectedIndex == -1)) {
            return null
        }
        if (tag == "select") {
            var index = el.selectedIndex;
            if (index < 0) {
                return null
            }
            var a = [],
                ops = el.options;
            var one = (t == "select-one");
            var max = (one
                ? index + 1
                : ops.length);
            for (var i = (one
                ? index
                : 0); i < max; i++) {
                var op = ops[i];
                if (op.selected) {
                    var v = op.value;
                    if (!v) {
                        v = (op.attributes && op.attributes.value && !(op.attributes.value.specified))
                            ? op.text
                            : op.value
                    }
                    if (one) {
                        return v
                    }
                    a.push(v)
                }
            }
            return a
        }
        return $(el).val()
    };
    $.fn.clearForm = function (includeHidden) {
        return this.each(function () {
            $("input,select,textarea", this).clearFields(includeHidden)
        })
    };
    $.fn.clearFields = $.fn.clearInputs = function (includeHidden) {
        var re = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
        return this.each(function () {
            var t = this.type,
                tag = this.tagName.toLowerCase();
            if (re.test(t) || tag == "textarea") {
                this.value = ""
            } else {
                if (t == "checkbox" || t == "radio") {
                    this.checked = false
                } else {
                    if (tag == "select") {
                        this.selectedIndex = -1
                    } else {
                        if (t == "file") {
                            if (/MSIE/.test(navigator.userAgent)) {
                                $(this).replaceWith($(this).clone(true))
                            } else {
                                $(this).val("")
                            }
                        } else {
                            if (includeHidden) {
                                if ((includeHidden === true && /hidden/.test(t)) || (typeof includeHidden == "string" && $(this).is(includeHidden))) {
                                    this.value = ""
                                }
                            }
                        }
                    }
                }
            }
        })
    };
    $.fn.resetForm = function () {
        return this.each(function () {
            if (typeof this.reset == "function" || (typeof this.reset == "object" && !this.reset.nodeType)) {
                this.reset()
            }
        })
    };
    $.fn.enable = function (b) {
        if (b === undefined) {
            b = true
        }
        return this.each(function () {
            this.disabled = !b
        })
    };
    $.fn.selected = function (select) {
        if (select === undefined) {
            select = true
        }
        return this.each(function () {
            var t = this.type;
            if (t == "checkbox" || t == "radio") {
                this.checked = select
            } else {
                if (this.tagName.toLowerCase() == "option") {
                    var $sel = $(this).parent("select");
                    if (select && $sel[0] && $sel[0].type == "select-one") {
                        $sel.find("option").selected(false)
                    }
                    this.selected = select
                }
            }
        })
    };
    $.fn.ajaxSubmit.debug = false;
    function log() {
        if (!$.fn.ajaxSubmit.debug) {
            return
        }
        var msg = "[jquery.form] " + Array.prototype.join.call(arguments, "");
        if (window.console && window.console.log) {
            window.console.log(msg)
        } else {
            if (window.opera && window.opera.postError) {
                window.opera.postError(msg)
            }
        }
    }
}));
/*! VALIDATE */
!function (a) {
    a.extend(a.fn, {
        validate: function (b) {
            if (!this.length) 
                return void(b && b.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing."));
            var c = a.data(this[0], "validator");
            return c
                ? c
                : (this.attr("novalidate", "novalidate"), c = new a.validator(b, this[0]), a.data(this[0], "validator", c), c.settings.onsubmit && (this.validateDelegate(":submit", "click", function (b) {
                    c.settings.submitHandler && (c.submitButton = b.target),
                    a(b.target).hasClass("cancel") && (c.cancelSubmit = !0),
                    void 0 !== a(b.target).attr("formnovalidate") && (c.cancelSubmit = !0)
                }), this.submit(function (b) {
                    function d() {
                        var d;
                        return c.settings.submitHandler
                            ? (c.submitButton && (d = a("<input type='hidden'/>").attr("name", c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm)),
                            c.settings.submitHandler.call(c, c.currentForm, b),
                            c.submitButton && d.remove(),
                            !1)
                            : !0
                }
                return c.settings.debug && b.preventDefault(), c.cancelSubmit
                    ? (c.cancelSubmit = !1, d())
                    : c.form()
                        ? c.pendingRequest
                            ? (c.formSubmitted = !0, !1)
                            : d()
                            : (c.focusInvalid(), !1)
            })), c)
        },
        valid: function () {
            var b,
                c;
            return a(this[0]).is("form")
                ? b = this.validate().form()
                : (b = !0, c = a(this[0].form).validate(), this.each(function () {
                    b = c.element(this) && b
                })), b
        },
        removeAttrs: function (b) {
            var c = {},
                d = this;
            return a.each(b.split(/\s/), function (a, b) {
                c[b] = d.attr(b),
                d.removeAttr(b)
            }), c
        },
        rules: function (b, c) {
            var d,
                e,
                f,
                g,
                h,
                i,
                j = this[0];
            if (b) switch (d = a.data(j.form, "validator").settings, e = d.rules, f = a.validator.staticRules(j), b) {
                case "add" :
                a.extend(f, a.validator.normalizeRule(c)), delete f.messages, e[j.name] = f, c.messages && (d.messages[j.name] = a.extend(d.messages[j.name], c.messages));
                break;
            case "remove" :
                return c
                    ? (i = {}, a.each(c.split(/\s/), function (b, c) {
                        i[c] = f[c],
                        delete f[c],
                        "required" === c && a(j).removeAttr("aria-required")
                    }), i)
                    : (delete e[j.name], f)
            }
            return g = a.validator.normalizeRules(a.extend({}, a.validator.classRules(j), a.validator.attributeRules(j), a.validator.dataRules(j), a.validator.staticRules(j)), j), g.required && (h = g.required, delete g.required, g = a.extend({
                required: h
            }, g), a(j).attr("aria-required", "true")), g.remote && (h = g.remote, delete g.remote, g = a.extend(g, {
                remote: h
            })), g
        }
    }),
    a.extend(a.expr[":"], {
        blank: function (b) {
            return !a.trim("" + a(b).val())
        },
        filled: function (b) {
            return !!a.trim("" + a(b).val())
        },
        unchecked: function (b) {
            return !a(b).prop("checked")
        }
    }),
    a.validator = function (b, c) {
        this.settings = a.extend(!0, {}, a.validator.defaults, b),
        this.currentForm = c,
        this.init()
    },
    a.validator.format = function (b, c) {
        return 1 === arguments.length
            ? function () {
                var c = a.makeArray(arguments);
                return c.unshift(b), a.validator.format.apply(this, c)
            }
            : (arguments.length > 2 && c.constructor !== Array && (c = a.makeArray(arguments).slice(1)),
            c.constructor !== Array && (c = [c]),
            a.each(c, function (a, c) {
                b = b.replace(new RegExp("\\{" + a + "\\}", "g"), function () {
                    return c
                })
            }),
            b)
    },
    a.extend(a.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusInvalid: !0,
            errorContainer: a([]),
            errorLabelContainer: a([]),
            onsubmit: !0,
            ignore: ":hidden",
            ignoreTitle: !1,
            onfocusin: function (a) {
                this.lastActive = a,
                this.settings.focusCleanup && !this.blockFocusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, a, this.settings.errorClass, this.settings.validClass),
                this.addWrapper(this.errorsFor(a)).hide())
            },
            onfocusout: function (a) {
                this.checkable(a) || !(a.name in this.submitted) && this.optional(a) || this.element(a)
            },
            onkeyup: function (a, b) {
                (9 !== b.which || "" !== this.elementValue(a)) && (a.name in this.submitted || a === this.lastElement) && this.element(a)
            },
            onclick: function (a) {
                a.name in this.submitted
                    ? this.element(a)
                    : a.parentNode.name in this.submitted && this.element(a.parentNode)
            },
            highlight: function (b, c, d) {
                "radio" === b.type
                    ? this.findByName(b.name).addClass(c).removeClass(d)
                    : a(b).addClass(c).removeClass(d)
            },
            unhighlight: function (b, c, d) {
                "radio" === b.type
                    ? this.findByName(b.name).removeClass(c).addClass(d)
                    : a(b).removeClass(c).addClass(d)
            }
        },
        setDefaults: function (b) {
            a.extend(a.validator.defaults, b)
        },
        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            maxlength: a.validator.format("Please enter no more than {0} characters."),
            minlength: a.validator.format("Please enter at least {0} characters."),
            rangelength: a.validator.format("Please enter a value between {0} and {1} characters long."),
            range: a.validator.format("Please enter a value between {0} and {1}."),
            max: a.validator.format("Please enter a value less than or equal to {0}."),
            min: a.validator.format("Please enter a value greater than or equal to {0}.")
        },
        autoCreateRanges: !1,
        prototype: {
            init: function () {
                function b(b) {
                    var c = a.data(this[0].form, "validator"),
                        d = "on" + b.type.replace(/^validate/, ""),
                        e = c.settings;
                    e[d] && !this.is(e.ignore) && e[d].call(c, this[0], b)
                }
                this.labelContainer = a(this.settings.errorLabelContainer),
                this.errorContext = this.labelContainer.length && this.labelContainer || a(this.currentForm),
                this.containers = a(this.settings.errorContainer).add(this.settings.errorLabelContainer),
                this.submitted = {},
                this.valueCache = {},
                this.pendingRequest = 0,
                this.pending = {},
                this.invalid = {},
                this.reset();
                var c,
                    d = this.groups = {};
                a.each(this.settings.groups, function (b, c) {
                    "string" == typeof c && (c = c.split(/\s/)),
                    a.each(c, function (a, c) {
                        d[c] = b
                    })
                }),
                c = this.settings.rules,
                a.each(c, function (b, d) {
                    c[b] = a.validator.normalizeRule(d)
                }),
                a(this.currentForm).validateDelegate(":text, [type='password'], [type='file'], select, textarea, [type='number'], [typ" +
                        "e='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [typ" +
                        "e='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local']",
                "focusin focusout keyup", b).validateDelegate("[type='radio'], [type='checkbox'], select, option", "click", b),
                this.settings.invalidHandler && a(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler),
                a(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required", "true")
            },
            form: function () {
                return this.checkForm(), a.extend(this.submitted, this.errorMap), this.invalid = a.extend({}, this.errorMap), this.valid() || a(this.currentForm).triggerHandler("invalid-form", [this]), this.showErrors(), this.valid()
            },
            checkForm: function () {
                this.prepareForm();
                for (var a = 0, b = this.currentElements = this.elements(); b[a]; a++) 
                    this.check(b[a]);
                return this.valid()
            },
            element: function (b) {
                var c = this.clean(b),
                    d = this.validationTargetFor(c),
                    e = !0;
                return this.lastElement = d, void 0 === d
                    ? delete this.invalid[c.name]
                    : (this.prepareElement(d), this.currentElements = a(d), e = this.check(d) !== !1, e
                        ? delete this.invalid[d.name]
                        : this.invalid[d.name] = !0), a(b).attr("aria-invalid", !e), this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)), this.showErrors(), e
            },
            showErrors: function (b) {
                if (b) {
                    a.extend(this.errorMap, b),
                    this.errorList = [];
                    for (var c in b) 
                        this.errorList.push({
                            message: b[c],
                            element: this.findByName(c)[0]
                        });
                    this.successList = a.grep(this.successList, function (a) {
                        return !(a.name in b)
                    })
                }
                this.settings.showErrors
                    ? this.settings.showErrors.call(this, this.errorMap, this.errorList)
                    : this.defaultShowErrors()
            },
            resetForm: function () {
                a.fn.resetForm && a(this.currentForm).resetForm(),
                this.submitted = {},
                this.lastElement = null,
                this.prepareForm(),
                this.hideErrors(),
                this.elements().removeClass(this.settings.errorClass).removeData("previousValue").removeAttr("aria-invalid")
            },
            numberOfInvalids: function () {
                return this.objectLength(this.invalid)
            },
            objectLength: function (a) {
                var b,
                    c = 0;
                for (b in a) 
                    c++;
                return c
            },
            hideErrors: function () {
                this.addWrapper(this.toHide).hide()
            },
            valid: function () {
                return 0 === this.size()
            },
            size: function () {
                return this.errorList.length
            },
            focusInvalid: function () {
                if (this.settings.focusInvalid) 
                    try {
                        a(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin")
                    } catch (b) {}
                },
            findLastActive: function () {
                var b = this.lastActive;
                return b && 1 === a.grep(this.errorList, function (a) {
                    return a.element.name === b.name
                }).length && b
            },
            elements: function () {
                var b = this,
                    c = {};
                return a(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function () {
                    return !this.name && b.settings.debug && window.console && console.error("%o has no name assigned", this), this.name in c || !b.objectLength(a(this).rules())
                        ? !1
                        : (c[this.name] = !0, !0)
                })
            },
            clean: function (b) {
                return a(b)[0]
            },
            errors: function () {
                var b = this.settings.errorClass.split(" ").join(".");
                return a(this.settings.errorElement + "." + b, this.errorContext)
            },
            reset: function () {
                this.successList = [],
                this.errorList = [],
                this.errorMap = {},
                this.toShow = a([]),
                this.toHide = a([]),
                this.currentElements = a([])
            },
            prepareForm: function () {
                this.reset(),
                this.toHide = this.errors().add(this.containers)
            },
            prepareElement: function (a) {
                this.reset(),
                this.toHide = this.errorsFor(a)
            },
            elementValue: function (b) {
                var c,
                    d = a(b),
                    e = d.attr("type");
                return "radio" === e || "checkbox" === e
                    ? a("input[name='" + d.attr("name") + "']:checked").val()
                    : (c = d.val(), "string" == typeof c
                        ? c.replace(/\r/g, "")
                        : c)
            },
            check: function (b) {
                b = this.validationTargetFor(this.clean(b));
                var c,
                    d,
                    e,
                    f = a(b).rules(),
                    g = a.map(f, function (a, b) {
                        return b
                    }).length,
                    h = !1,
                    i = this.elementValue(b);
                for (d in f) {
                    e = {
                        method: d,
                        parameters: f[d]
                    };
                    try {
                        if (c = a.validator.methods[d].call(this, i, b, e.parameters), "dependency-mismatch" === c && 1 === g) {
                            h = !0;
                            continue
                        }
                        if (h = !1, "pending" === c) 
                            return void(this.toHide = this.toHide.not(this.errorsFor(b)));
                        if (!c) 
                            return this.formatAndAdd(b, e), !1
                    } catch (j) {
                        throw this.settings.debug && window.console && console.log("Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method.", j),
                        j
                    }
                }
                if (!h) 
                    return this.objectLength(f) && this.successList.push(b), !0
            },
            customDataMessage: function (b, c) {
                return a(b).data("msg" + c[0].toUpperCase() + c.substring(1).toLowerCase()) || a(b).data("msg")
            },
            customMessage: function (a, b) {
                var c = this.settings.messages[a];
                return c && (c.constructor === String
                    ? c
                    : c[b])
            },
            findDefined: function () {
                for (var a = 0; a < arguments.length; a++) 
                    if (void 0 !== arguments[a]) 
                        return arguments[a];
                    return void 0
            },
            defaultMessage: function (b, c) {
                return this.findDefined(this.customMessage(b.name, c), this.customDataMessage(b, c), !this.settings.ignoreTitle && b.title || void 0, a.validator.messages[c], "<strong>Warning: No message defined for " + b.name + "</strong>")
            },
            formatAndAdd: function (b, c) {
                var d = this.defaultMessage(b, c.method),
                    e = /\$?\{(\d+)\}/g;
                "function" == typeof d
                    ? d = d.call(this, c.parameters, b)
                    : e.test(d) && (d = a.validator.format(d.replace(e, "{$1}"), c.parameters)),
                this.errorList.push({
                    message: d,
                    element: b,
                    method: c.method
                }),
                this.errorMap[b.name] = d,
                this.submitted[b.name] = d
            },
            addWrapper: function (a) {
                return this.settings.wrapper && (a = a.add(a.parent(this.settings.wrapper))), a
            },
            defaultShowErrors: function () {
                var a,
                    b,
                    c;
                for (a = 0; this.errorList[a]; a++) 
                    c = this.errorList[a],
                    this.settings.highlight && this.settings.highlight.call(this, c.element, this.settings.errorClass, this.settings.validClass),
                    this.showLabel(c.element, c.message);
                if (this.errorList.length && (this.toShow = this.toShow.add(this.containers)),
                this.settings.success) 
                    for (a = 0; this.successList[a]; a++) 
                        this.showLabel(this.successList[a]);
                    if (this.settings.unhighlight) 
                        for (a = 0, b = this.validElements(); b[a]; a++) 
                            this.settings.unhighlight.call(this, b[a], this.settings.errorClass, this.settings.validClass);
                        this.toHide = this.toHide.not(this.toShow),
                        this.hideErrors(),
                        this.addWrapper(this.toShow).show()
                }
            ,
            validElements: function () {
                return this.currentElements.not(this.invalidElements())
            },
            invalidElements: function () {
                return a(this.errorList).map(function () {
                    return this.element
                })
            },
            showLabel: function (b, c) {
            var d = this.errorsFor(b);
                d.length
                    ? (d.removeClass(this.settings.validClass).addClass(this.settings.errorClass), d.html(c))
                    : (d = a("<" + this.settings.errorElement + ">").attr("for", this.idOrName(b)).addClass(this.settings.errorClass).html(c || ""), this.settings.wrapper && (d = d.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()), this.labelContainer.append(d).length || (this.settings.errorPlacement
                        ? this.settings.errorPlacement(d, a(b))
                        : d.insertAfter(b))),
                !c && this.settings.success && (d.text(""), "string" == typeof this.settings.success
                    ? d.addClass(this.settings.success)
                    : this.settings.success(d, b)),
                this.toShow = this.toShow.add(d)
            },
            errorsFor: function (b) {
                var c = this.idOrName(b);
                return this.errors().filter(function () {
                    return a(this).attr("for") === c
                })
            },
            idOrName: function (a) {
                return this.groups[a.name] || (this.checkable(a)
                    ? a.name
                    : a.id || a.name)
            },
            validationTargetFor: function (a) {
                return this.checkable(a) && (a = this.findByName(a.name).not(this.settings.ignore)[0]), a
            },
            checkable: function (a) {
                return /radio|checkbox/i.test(a.type)
            },
            findByName: function (b) {
                return a(this.currentForm).find("[name='" + b + "']")
            },
            getLength: function (b, c) {
                switch (c.nodeName.toLowerCase()) {
                case "select" :
                    return a("option:selected", c).length;
                case "input" :
                    if (this.checkable(c)) 
                        return this.findByName(c.name).filter(":checked").length
                }
                return b.length
            },
            depend: function (a, b) {
                return this.dependTypes[typeof a]
                    ? this.dependTypes[typeof a](a, b)
                    : !0
            },
            dependTypes: {
                "boolean": function (a) {
                    return a
                },
                string: function (b, c) {
                    return !!a(b, c.form).length
                },
                "function": function (a, b) {
                    return a(b)
                }
            },
            optional: function (b) {
                var c = this.elementValue(b);
                return !a.validator.methods.required.call(this, c, b) && "dependency-mismatch"
            },
            startRequest: function (a) {
                this.pending[a.name] || (this.pendingRequest++, this.pending[a.name] = !0)
            },
            stopRequest: function (b, c) {
                this.pendingRequest--,
                this.pendingRequest < 0 && (this.pendingRequest = 0),
                delete this.pending[b.name],
                c && 0 === this.pendingRequest && this.formSubmitted && this.form()
                    ? (a(this.currentForm).submit(), this.formSubmitted = !1)
                    : !c && 0 === this.pendingRequest && this.formSubmitted && (a(this.currentForm).triggerHandler("invalid-form", [this]), this.formSubmitted = !1)
            },
            previousValue: function (b) {
                return a.data(b, "previousValue") || a.data(b, "previousValue", {
                    old: null,
                    valid: !0,
                    message: this.defaultMessage(b, "remote")
                })
            }
        },
        classRuleSettings: {
            required: {
                required: !0
            },
            email: {
                email: !0
            },
            url: {
                url: !0
            },
            date: {
                date: !0
            },
            dateISO: {
                dateISO: !0
            },
            number: {
                number: !0
            },
            digits: {
                digits: !0
            },
            creditcard: {
                creditcard: !0
            }
        },
        addClassRules: function (b, c) {
            b.constructor === String
                ? this.classRuleSettings[b] = c
                : a.extend(this.classRuleSettings, b)
        },
        classRules: function (b) {
            var c = {},
                d = a(b).attr("class");
            return d && a.each(d.split(" "), function () {
                this in a.validator.classRuleSettings && a.extend(c, a.validator.classRuleSettings[this])
            }), c
        },
        attributeRules: function (b) {
            var c,
                d,
                e = {},
                f = a(b),
                g = b.getAttribute("type");
            for (c in a.validator.methods) 
                "required" === c
                    ? (d = b.getAttribute(c), "" === d && (d = !0), d = !!d)
                    : d = f.attr(c),
                /min|max/.test(c) && (null === g || /number|range|text/.test(g)) && (d = Number(d)),
                d || 0 === d
                    ? e[c] = d
                    : g === c && "range" !== g && (e[c] = !0);
            return e.maxlength && /-1|2147483647|524288/.test(e.maxlength) && delete e.maxlength, e
        },
        dataRules: function (b) {
            var c,
                d,
                e = {},
                f = a(b);
            for (c in a.validator.methods) 
                d = f.data("rule" + c[0].toUpperCase() + c.substring(1).toLowerCase()),
                void 0 !== d && (e[c] = d);
            return e
        },
        staticRules: function (b) {
            var c = {},
                d = a.data(b.form, "validator");
            return d.settings.rules && (c = a.validator.normalizeRule(d.settings.rules[b.name]) || {}), c
        },
        normalizeRules: function (b, c) {
            return a.each(b, function (d, e) {
                if (e === !1) 
                    return void delete b[d];
                if (e.param || e.depends) {
                    var f = !0;
                    switch (typeof e.depends) {
                    case "string" :
                        f = !!a(e.depends, c.form).length;
                        break;
                    case "function" :
                        f = e.depends.call(c, c)
                    }
                    f
                        ? b[d] = void 0 !== e.param
                            ? e.param
                            : !0
                            : delete b[d]
                }
            }), a.each(b, function (d, e) {
                b[d] = a.isFunction(e)
                    ? e(c)
                    : e
            }), a.each(["minlength", "maxlength"], function () {
                b[this] && (b[this] = Number(b[this]))
            }), a.each(["rangelength", "range"], function () {
                var c;
                b[this] && (a.isArray(b[this])
                    ? b[this] = [
                        Number(b[this][0]), Number(b[this][1])
                    ]
                    : "string" == typeof b[this] && (c = b[this].split(/[\s,]+/), b[this] = [
                        Number(c[0]), Number(c[1])
                    ]))
            }), a.validator.autoCreateRanges && (b.min && b.max && (b.range = [
                b.min, b.max
            ], delete b.min, delete b.max),
            b.minlength && b.maxlength && (b.rangelength = [
                b.minlength, b.maxlength
            ], delete b.minlength, delete b.maxlength)), b
        },
        normalizeRule: function (b) {
            if ("string" == typeof b) {
                var c = {};
                a.each(b.split(/\s/), function () {
                    c[this] = !0
                }),
                b = c
            }
            return b
        },
        addMethod: function (b, c, d) {
            a.validator.methods[b] = c,
            a.validator.messages[b] = void 0 !== d
                ? d
                : a.validator.messages[b],
            c.length < 3 && a.validator.addClassRules(b, a.validator.normalizeRule(b))
        },
        methods: {
            required: function (b, c, d) {
                if (!this.depend(d, c)) 
                    return "dependency-mismatch";
                if ("select" === c.nodeName.toLowerCase()) {
                    var e = a(c).val();
                    return e && e.length > 0
                }
                return this.checkable(c)
                    ? this.getLength(b, c) > 0
                    : a.trim(b).length > 0
            },
            email: function (a, b) {
                return this.optional(b) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a)
            },
            url: function (a, b) {
                return this.optional(b) || /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a)
            },
            date: function (a, b) {
                return this.optional(b) || !/Invalid|NaN/.test(new Date(a).toString())
            },
            dateISO: function (a, b) {
                return this.optional(b) || /^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/.test(a)
            },
            number: function (a, b) {
                return this.optional(b) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a)
            },
            digits: function (a, b) {
                return this.optional(b) || /^\d+$/.test(a)
            },
            creditcard: function (a, b) {
                if (this.optional(b)) 
                    return "dependency-mismatch";
                if (/[^0-9 \-]+/.test(a)) 
                    return !1;
                var c,
                    d,
                    e = 0,
                    f = 0,
                    g = !1;
                if (a = a.replace(/\D/g, ""), a.length < 13 || a.length > 19) 
                    return !1;
                for (c = a.length - 1; c >= 0; c--) 
                    d = a.charAt(c),
                    f = parseInt(d, 10),
                    g && (f *= 2) > 9 && (f -= 9),
                    e += f,
                    g = !g;
                return e % 10 === 0
            },
            minlength: function (b, c, d) {
                var e = a.isArray(b)
                    ? b.length
                    : this.getLength(a.trim(b), c);
                return this.optional(c) || e >= d
            },
            maxlength: function (b, c, d) {
                var e = a.isArray(b)
                    ? b.length
                    : this.getLength(a.trim(b), c);
                return this.optional(c) || d >= e
            },
            rangelength: function (b, c, d) {
                var e = a.isArray(b)
                    ? b.length
                    : this.getLength(a.trim(b), c);
                return this.optional(c) || e >= d[0] && e <= d[1]
            },
            min: function (a, b, c) {
                return this.optional(b) || a >= c
            },
            max: function (a, b, c) {
                return this.optional(b) || c >= a
            },
            range: function (a, b, c) {
                return this.optional(b) || a >= c[0] && a <= c[1]
            },
            equalTo: function (b, c, d) {
                var e = a(d);
                return this.settings.onfocusout && e.unbind(".validate-equalTo").bind("blur.validate-equalTo", function () {
                    a(c).valid()
                }), b === e.val()
            },
            remote: function (b, c, d) {
                if (this.optional(c)) 
                    return "dependency-mismatch";
                var e,
                    f,
                    g = this.previousValue(c);
                return this.settings.messages[c.name] || (this.settings.messages[c.name] = {}), g.originalMessage = this.settings.messages[c.name].remote, this.settings.messages[c.name].remote = g.message, d = "string" == typeof d && {
                    url: d
                } || d, g.old === b
                    ? g.valid
                    : (g.old = b, e = this, this.startRequest(c), f = {}, f[c.name] = b, a.ajax(a.extend(!0, {
                        url: d,
                        mode: "abort",
                        port: "validate" + c.name,
                        dataType: "json",
                        data: f,
                        context: e.currentForm,
                        success: function (d) {
                            var f,
                                h,
                                i,
                                j = d === !0 || "true" === d;
                            e.settings.messages[c.name].remote = g.originalMessage,
                            j
                                ? (i = e.formSubmitted, e.prepareElement(c), e.formSubmitted = i, e.successList.push(c), delete e.invalid[c.name], e.showErrors())
                                : (f = {}, h = d || e.defaultMessage(c, "remote"), f[c.name] = g.message = a.isFunction(h)
                                    ? h(b)
                                    : h, e.invalid[c.name] = !0, e.showErrors(f)),
                        g.valid = j,
                        e.stopRequest(c, j)
                    }
                }, d)), "pending")
            }
        }
    }),
    a.format = function () {
        throw "$.format has been deprecated. Please use $.validator.format instead."
    }
}(jQuery),

function (a) {
    a.extend(a.fn, {
        validateDelegate: function (b, c, d) {
            return this.bind(c, function (c) {
                var e = a(c.target);
                return e.is(b)
                    ? d.apply(e, arguments)
                    : void 0
            })
        }
    })
}(jQuery);

/*! SLIDER */
(function (window, $) {
    plugin = 'superslides';
    $.fn[plugin] = function (option, args) {
        var result = [];
    };
})(this, jQuery);

/*! POOYAA PLUGINS */
(function ($, window, document, undefined) {
    var pluginName = 'pofloor';
    $.fn[pluginName] = function (options) {
        this.each(function () {
            if (!$.data(this, pluginName)) {
                $.data(this, pluginName, new Plugin(this, options));
            }
        });
        return this;
    };
})(jQuery, window, document);;

