/* =========================================
OSCAR - Responsive App Landing Page - Main javascript
========================================= */
var $ = jQuery.noConflict();
jQuery(function($) {
	"use strict";
    var oscar = {
 
    /*** Init Function ***/
    init: function() {
        oscar.Preloader();
		oscar.Navigation();
		oscar.Scroll();
		oscar.Parallax();
        oscar.Animations();
		oscar.Carousel();
        oscar.Lightbox();
    },
	
	/*** Preloader ***/
    Preloader: function() {
        $(window).load(function() {
            $('.loading').delay(100).fadeOut('slow');
            $('#preloader').delay(500).fadeOut('slow');
            //setTimeout(function(){$('.logo').addClass('animated fadeInUp')},1000);
			//setTimeout(function(){$('.home h1').addClass('animated fadeInUp')},1300);
			//setTimeout(function(){$('.home h5,.home .contact').addClass('animated fadeInUp')},1600);
			//setTimeout(function(){$('.home .home-screenshot').addClass('animated fadeInRight')},1400);
//			setTimeout(function(){$('.home .button-large,.intro .button,.home form').addClass('animated bounceIn')},2100);
        })
    },
	
	/*** Navigation ***/
    Navigation: function() {
		 //$("#navigation").sticky({topSpacing:0});
	},
	
	/*** Scroll ***/
    Scroll: function() {
		$('a[href*=#]:not([href=#])').bind('click',function(event){
            /*
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $anchor.offset().top -0
            }, 1500,'easeInOutExpo');
            event.preventDefault();
            */
		});
	},

	/*** Carousel ***/
    Carousel: function() {
        $('#owl-gallery').owlCarousel({
            items : 4,
            itemsDesktop : [1199,5],
            itemsDesktopSmall : [980,4],
            itemsTablet: [768,3],
            itemsTabletSmall: [550,2],
            itemsMobile : [480,2]
        });
    },

	/*** Lightbox ***/
    Lightbox: function() {
        $('#owl-gallery a').nivoLightbox({
            effect: 'fall'
        });
    },

	/*** Parallax ***/
	Parallax: function(){
		$(window).bind('load', function () {parallaxInit();});
		function parallaxInit() {
			$('#home').parallax("30%", 0.1);
		}
	},

    /*** Animations ***/
    Animations: function() {
		
		$('#home').waypoint(function() {
			setTimeout(function(){$('.home .site_title').addClass('animated fadeInDown')},300);
			setTimeout(function(){$('.home .site_functions').addClass('animated fadeInDown')},600);
			setTimeout(function(){$('.home .button_signup').addClass('animated fadeInDown')},900);
        }, { offset: '50%' });
        
    }
 }
$(function() {oscar.init();});

	
	/*** QUOTE SLIDER ***/
	$("#quote-slider").each(function() {
		$("#quote-slider").sudoSlider({
			customLink:'a.quoteLink',
			speed: 400,
			responsive: true,
			prevNext: true, // Set this to false if you only want to show one quote
			prevHtml: '<a href="#" class="quote-arrow-prev"><i class="arrow_carrot-left_alt2"></i></a>', 
			nextHtml: '<a href="#" class="quote-arrow-next"><i class="arrow_carrot-right_alt2"></i></a>', 
			auto:false,
			useCSS: true,
			continuous: true,
			updateBefore: true
		});
	});

	/*** SUBCRIBE FORM ***/
	$("#subscribe").each(function() {
		$('#subscribe').ajaxForm( {
			target: '#newsletterform .message',
			success: function() { 
				$('#subscribe').slideUp('slow');
				$("#newsletterform .waiting").delay(100).fadeIn("slow");
				$("#newsletterform .waiting").delay(500).fadeOut("slow");
				$("#newsletterform .message").delay(1500).slideDown("slow");
			}
		});
	});
	
	/*** CONTACT FORM ***/
	//$("#contactform").each(function() {
//		$("#contactform").submit(function(e) {
//			e.preventDefault();
//			$.ajax({
//				url: "inc/contact.php",
//				data: "name="+ escape($("#contactName").val()) +"&email=" + escape($("#contactEmail").val()) + "&phone=" + escape($("#contactPhone").val()) + "&contents=" + escape($("#contactContents").val()) + "&message="+escape($("#contactMessage").val()),
//				dataType: "json",
//				success: function(resp) {
//					$("#contactName, #contactEmail, #contactMessage").removeClass("error");
//					if(resp.success == 1){
//						$(".contact .waiting").delay(100).fadeIn("slow");
//						$(".contact .waiting").delay(500).fadeOut("slow");
//						$(".contact .message").delay(1500).slideDown("slow");
//						$(".contact .message").delay(2000).html("<p>We'll be in touch real soon!</p>").fadeIn("slow");
//						$("#contactform").slideUp("slow");
//						$("#contactName, #contactEmail, #contactMessage, #contactPhone").val("");
//					}
//					else {
//						if(resp.errorCode == 1){
//							$("#contactName").addClass("error").focus();
//						}
//						else if(resp.errorCode == 2){
//							$("#contactEmail").addClass("error").focus();
//						}
//						else if(resp.errorCode == 3){
//							$("#contactMessage").addClass("error").focus();
//						}	
//					}					
//				}
//			});
//			return false;
//		});
//	});
	
});



/**
 * Vertically center Bootstrap 3 modals so they aren't always stuck at the top
 */
$(function() {
    function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        
        // Dividing by two centers the modal exactly, but dividing by three 
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
});


/**
 * Tooltip
 */
$(document).ready(function() {
	$('.tooltip_mssg').tooltip({
		container: 'body'
	});
});


/**
 * Select
 */
$(function(){
	$('select').select2({
		minimumResultsForSearch: Infinity
	});
});


/**
 * Resize height of home
 */
$(document).ready(function() {
    resizeHeaderDiv();
    $(window).resize(resizeHeaderDiv);
});

function resizeHeaderDiv(){
    var wh = $(window).height();
		$('header.home').css({
		   minHeight: wh+'px'
    });
		$('header.home #homescreen').css({
		   minHeight: wh+'px'
    });
}


initialize();

