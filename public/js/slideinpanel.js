jQuery(document).ready(function($){
	//open the lateral panel
	$('.cd-btn').on('click', function(event){
		event.preventDefault();
		$('.cd-panel').addClass('is-visible');
		$('.chat_trigger').hide();
	});
	//clode the lateral panel
	$('.cd-panel').on('click', function(event){
		if( $(event.target).is('.cd-panel') ) { 
			$('.cd-panel').removeClass('is-visible');
			$('.chat_trigger').show();
			event.preventDefault();
		}
	});
	$('.cd-panel-close').on('click', function(event){
		event.preventDefault();
		$('.cd-panel').removeClass('is-visible');
		$('.chat_trigger').show();
		$('.chat_trigger .chat_message').show();
		$('.chat-button').removeClass('minify');
	});
	//minimize the lateral panel
	$('.minify_chat').on('click', function(event){
		event.preventDefault();
		$('.cd-panel').removeClass('is-visible');
		$('.chat_trigger').show();
		$('.chat_trigger .chat_message').hide();
		$('.chat-button').addClass('minify');
	});
	
});