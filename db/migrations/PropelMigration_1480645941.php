<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1480645941.
 * Generated on 2016-12-02 11:32:21 by developer
 */
class PropelMigration_1480645941
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `message_to_users`;

DROP TABLE IF EXISTS `test`;

DROP TABLE IF EXISTS `tmp_users`;

ALTER TABLE `access_tokens` DROP FOREIGN KEY `access_tokens_ibfk_1`;

ALTER TABLE `access_tokens`

  CHANGE `access_token` `access_token` VARCHAR(40) NOT NULL,

  CHANGE `client_id` `client_id` VARCHAR(80) NOT NULL;

ALTER TABLE `access_tokens` ADD CONSTRAINT `access_tokens_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `attributes` DROP FOREIGN KEY `attributes_ibfk_1`;

ALTER TABLE `attributes` ADD CONSTRAINT `attributes_ibfk_1`
    FOREIGN KEY (`attribute_group_id`)
    REFERENCES `attribute_groups` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `authorization_codes` DROP FOREIGN KEY `authorization_codes_ibfk_1`;

DROP INDEX `client_id` ON `authorization_codes`;

ALTER TABLE `authorization_codes`

  CHANGE `authorization_code` `authorization_code` VARCHAR(40) NOT NULL,

  CHANGE `client_id` `client_id` VARCHAR(80) NOT NULL;

CREATE INDEX `authorization_codes_ibfk_1` ON `authorization_codes` (`client_id`);

ALTER TABLE `authorization_codes` ADD CONSTRAINT `authorization_codes_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `clients`

  CHANGE `client_id` `client_id` VARCHAR(80) NOT NULL;

ALTER TABLE `messages`

  ADD `to_uids` VARCHAR(255) AFTER `from_uid`;

ALTER TABLE `public_keys` DROP FOREIGN KEY `public_keys_ibfk_1`;

ALTER TABLE `public_keys`

  DROP PRIMARY KEY,

  CHANGE `client_id` `client_id` VARCHAR(80);

CREATE INDEX `public_keys_ibfk_1` ON `public_keys` (`client_id`);

ALTER TABLE `public_keys` ADD CONSTRAINT `public_keys_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `refresh_tokens` DROP FOREIGN KEY `refresh_tokens_ibfk_1`;

DROP INDEX `client_id` ON `refresh_tokens`;

ALTER TABLE `refresh_tokens`

  CHANGE `client_id` `client_id` VARCHAR(80) NOT NULL;

CREATE INDEX `refresh_tokens_ibfk_1` ON `refresh_tokens` (`client_id`);

ALTER TABLE `refresh_tokens` ADD CONSTRAINT `refresh_tokens_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `user_attribute_values`

  ADD `user_attribute_value_set_id` BIGINT NOT NULL AFTER `attribute_id`;

CREATE INDEX `user_attribute_values_ibfk_3` ON `user_attribute_values` (`user_attribute_value_set_id`, `modified_at`);

ALTER TABLE `user_attribute_values` ADD CONSTRAINT `user_attribute_values_ibfk_3`
    FOREIGN KEY (`user_attribute_value_set_id`)
    REFERENCES `user_attribute_value_sets` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

CREATE TABLE `attribute_origins`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `attribute_origin_name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `user_attribute_value_sets`
(
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `attribute_origin_id` INTEGER NOT NULL,
    `created_at` DATETIME NOT NULL,
    `modified_at` DATETIME NOT NULL,
    `deleted_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `user_attribute_value_sets_ibfk_1` (`attribute_origin_id`),
    CONSTRAINT `user_attribute_value_sets_ibfk_1`
        FOREIGN KEY (`attribute_origin_id`)
        REFERENCES `attribute_origins` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `attribute_origins`;

DROP TABLE IF EXISTS `user_attribute_value_sets`;

ALTER TABLE `access_tokens` DROP FOREIGN KEY `access_tokens_ibfk_1`;

ALTER TABLE `access_tokens`

  CHANGE `access_token` `access_token` VARCHAR(40) DEFAULT \'\' NOT NULL,

  CHANGE `client_id` `client_id` VARCHAR(80) DEFAULT \'\' NOT NULL;

ALTER TABLE `access_tokens` ADD CONSTRAINT `access_tokens_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`);

ALTER TABLE `attributes` DROP FOREIGN KEY `attributes_ibfk_1`;

ALTER TABLE `attributes` ADD CONSTRAINT `attributes_ibfk_1`
    FOREIGN KEY (`attribute_group_id`)
    REFERENCES `attribute_groups` (`id`);

ALTER TABLE `authorization_codes` DROP FOREIGN KEY `authorization_codes_ibfk_1`;

DROP INDEX `authorization_codes_ibfk_1` ON `authorization_codes`;

ALTER TABLE `authorization_codes`

  CHANGE `authorization_code` `authorization_code` VARCHAR(40) DEFAULT \'\' NOT NULL,

  CHANGE `client_id` `client_id` VARCHAR(80) DEFAULT \'\' NOT NULL;

CREATE INDEX `client_id` ON `authorization_codes` (`client_id`);

ALTER TABLE `authorization_codes` ADD CONSTRAINT `authorization_codes_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`);

ALTER TABLE `clients`

  CHANGE `client_id` `client_id` VARCHAR(80) DEFAULT \'\' NOT NULL;

ALTER TABLE `messages`

  DROP `to_uids`;

ALTER TABLE `public_keys` DROP FOREIGN KEY `public_keys_ibfk_1`;

DROP INDEX `public_keys_ibfk_1` ON `public_keys`;

ALTER TABLE `public_keys`

  CHANGE `client_id` `client_id` VARCHAR(80) DEFAULT \'\' NOT NULL,

  ADD PRIMARY KEY (`client_id`);

ALTER TABLE `public_keys` ADD CONSTRAINT `public_keys_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`);

ALTER TABLE `refresh_tokens` DROP FOREIGN KEY `refresh_tokens_ibfk_1`;

DROP INDEX `refresh_tokens_ibfk_1` ON `refresh_tokens`;

ALTER TABLE `refresh_tokens`

  CHANGE `client_id` `client_id` VARCHAR(80) DEFAULT \'\' NOT NULL;

CREATE INDEX `client_id` ON `refresh_tokens` (`client_id`);

ALTER TABLE `refresh_tokens` ADD CONSTRAINT `refresh_tokens_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`);

ALTER TABLE `user_attribute_values` DROP FOREIGN KEY `user_attribute_values_ibfk_3`;

DROP INDEX `user_attribute_values_ibfk_3` ON `user_attribute_values`;

ALTER TABLE `user_attribute_values`

  DROP `user_attribute_value_set_id`;

CREATE TABLE `message_to_users`
(
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `message_id` TINYINT,
    `uid` VARCHAR(255),
    PRIMARY KEY (`id`),
    INDEX `message_to_users_message_id` (`message_id`)
) ENGINE=InnoDB;

CREATE TABLE `test`
(
    `col1` VARCHAR(255)
) ENGINE=InnoDB;

CREATE TABLE `tmp_users`
(
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `uid` VARCHAR(255) NOT NULL,
    `username` VARCHAR(255),
    `password` VARCHAR(255),
    `email` VARCHAR(255),
    `email_verified` TINYINT(1) DEFAULT 0 NOT NULL,
    `first_name` VARCHAR(255) NOT NULL,
    `middle_name` VARCHAR(255),
    `last_name` VARCHAR(255) NOT NULL,
    `given_name` VARCHAR(255),
    `nickname` VARCHAR(255),
    `preferred_username` VARCHAR(255),
    `profile` VARCHAR(255),
    `picture` VARCHAR(255),
    `website` VARCHAR(255),
    `gender` VARCHAR(16),
    `birth_date` DATE,
    `zoneinfo` VARCHAR(16),
    `locale` VARCHAR(8),
    `phone_number` VARCHAR(128),
    `phone_number_verified` VARCHAR(128),
    `address` TEXT,
    `created_at` DATETIME NOT NULL,
    `modified_at` DATETIME NOT NULL,
    `deleted_at` DATETIME,
    `scope` TEXT,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}