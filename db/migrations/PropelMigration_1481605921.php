<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1481605921.
 * Generated on 2016-12-13 05:12:01 by Marcos
 */
class PropelMigration_1481605921
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `access_tokens` DROP FOREIGN KEY `access_tokens_ibfk_1`;

DROP INDEX `client_id` ON `access_tokens`;

CREATE INDEX `access_tokens_fk_1` ON `access_tokens` (`client_id`);

ALTER TABLE `access_tokens` ADD CONSTRAINT `access_tokens_fk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `attribute_sets` DROP FOREIGN KEY `attribute_sets_ibfk_1`;

DROP INDEX `attribute_sets_ibfk_1` ON `attribute_sets`;

CREATE INDEX `attribute_sets_fk_1` ON `attribute_sets` (`attribute_origin_id`);

ALTER TABLE `attribute_sets` ADD CONSTRAINT `attribute_sets_fk_1`
    FOREIGN KEY (`attribute_origin_id`)
    REFERENCES `attribute_origins` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `attributes` DROP FOREIGN KEY `attributes_ibfk_1`;

DROP INDEX `attribute_group_id` ON `attributes`;

CREATE INDEX `attributes_fk_1` ON `attributes` (`attribute_group_id`);

ALTER TABLE `attributes` ADD CONSTRAINT `attributes_fk_1`
    FOREIGN KEY (`attribute_group_id`)
    REFERENCES `attribute_groups` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `authorization_codes` DROP FOREIGN KEY `authorization_codes_ibfk_1`;

DROP INDEX `authorization_codes_ibfk_1` ON `authorization_codes`;

CREATE INDEX `authorization_codes_fk_1` ON `authorization_codes` (`client_id`);

ALTER TABLE `authorization_codes` ADD CONSTRAINT `authorization_codes_fk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `public_keys` DROP FOREIGN KEY `public_keys_ibfk_1`;

DROP INDEX `public_keys_ibfk_1` ON `public_keys`;

CREATE INDEX `public_keys_fk_1` ON `public_keys` (`client_id`);

ALTER TABLE `public_keys` ADD CONSTRAINT `public_keys_fk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `refresh_tokens` DROP FOREIGN KEY `refresh_tokens_ibfk_1`;

DROP INDEX `refresh_tokens_ibfk_1` ON `refresh_tokens`;

CREATE INDEX `refresh_tokens_fk_1` ON `refresh_tokens` (`client_id`);

ALTER TABLE `refresh_tokens` ADD CONSTRAINT `refresh_tokens_fk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `user_attribute_values` DROP FOREIGN KEY `user_attribute_values_ibfk_1`;

ALTER TABLE `user_attribute_values` DROP FOREIGN KEY `user_attribute_values_ibfk_2`;

DROP INDEX `user_attribute_values_ibfk_1` ON `user_attribute_values`;

DROP INDEX `user_attribute_values_ibfk_2` ON `user_attribute_values`;

DROP INDEX `user_attribute_values_ibfk_3` ON `user_attribute_values`;

CREATE INDEX `user_attribute_values_fk_1` ON `user_attribute_values` (`user_id`);

CREATE INDEX `user_attribute_values_fk_2` ON `user_attribute_values` (`attribute_id`, `created_at`);

CREATE INDEX `user_attribute_values_fk_3` ON `user_attribute_values` (`attribute_set_id`, `created_at`);

ALTER TABLE `user_attribute_values` ADD CONSTRAINT `user_attribute_values_fk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `user_attribute_values` ADD CONSTRAINT `user_attribute_values_fk_2`
    FOREIGN KEY (`attribute_id`)
    REFERENCES `attributes` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `user_attribute_values` ADD CONSTRAINT `user_attribute_values_fk_3`
    FOREIGN KEY (`attribute_set_id`)
    REFERENCES `attribute_sets` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

CREATE TABLE `label_groups`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `label_group_name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `labels`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `label_group_id` INTEGER NOT NULL,
    `label_type` VARCHAR(32) NOT NULL,
    `label_name` VARCHAR(255) NOT NULL,
    `is_multiple_allowed` TINYINT(1) DEFAULT 0 NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `labels_fk_1` (`label_group_id`),
    CONSTRAINT `labels_fk_1`
        FOREIGN KEY (`label_group_id`)
        REFERENCES `label_groups` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `label_origins`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `label_origin_name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `label_sets`
(
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `label_origin_id` INTEGER NOT NULL,
    `created_at` DATETIME NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `label_sets_fk_1` (`label_origin_id`),
    CONSTRAINT `label_sets_fk_1`
        FOREIGN KEY (`label_origin_id`)
        REFERENCES `label_origins` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `message_label_values`
(
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `message_id` BIGINT NOT NULL,
    `label_id` INTEGER NOT NULL,
    `label_set_id` BIGINT NOT NULL,
    `boolean_value` TINYINT(1),
    `double_value` DOUBLE,
    `string_value` TEXT,
    `timestamp_value` DATETIME,
    `effective_from` DATETIME,
    `effective_to` DATETIME,
    `created_at` DATETIME NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `message_label_values_fk_1` (`message_id`),
    INDEX `message_label_values_fk_2` (`label_id`, `created_at`),
    INDEX `message_label_values_fk_3` (`label_set_id`, `created_at`),
    CONSTRAINT `message_label_values_fk_1`
        FOREIGN KEY (`message_id`)
        REFERENCES `messages` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `message_label_values_fk_2`
        FOREIGN KEY (`label_id`)
        REFERENCES `labels` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `message_label_values_fk_3`
        FOREIGN KEY (`label_set_id`)
        REFERENCES `label_sets` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `label_groups`;

DROP TABLE IF EXISTS `labels`;

DROP TABLE IF EXISTS `label_origins`;

DROP TABLE IF EXISTS `label_sets`;

DROP TABLE IF EXISTS `message_label_values`;

ALTER TABLE `access_tokens` DROP FOREIGN KEY `access_tokens_fk_1`;

DROP INDEX `access_tokens_fk_1` ON `access_tokens`;

CREATE INDEX `client_id` ON `access_tokens` (`client_id`);

ALTER TABLE `access_tokens` ADD CONSTRAINT `access_tokens_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `attribute_sets` DROP FOREIGN KEY `attribute_sets_fk_1`;

DROP INDEX `attribute_sets_fk_1` ON `attribute_sets`;

CREATE INDEX `attribute_sets_ibfk_1` ON `attribute_sets` (`attribute_origin_id`);

ALTER TABLE `attribute_sets` ADD CONSTRAINT `attribute_sets_ibfk_1`
    FOREIGN KEY (`attribute_origin_id`)
    REFERENCES `attribute_origins` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `attributes` DROP FOREIGN KEY `attributes_fk_1`;

DROP INDEX `attributes_fk_1` ON `attributes`;

CREATE INDEX `attribute_group_id` ON `attributes` (`attribute_group_id`);

ALTER TABLE `attributes` ADD CONSTRAINT `attributes_ibfk_1`
    FOREIGN KEY (`attribute_group_id`)
    REFERENCES `attribute_groups` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `authorization_codes` DROP FOREIGN KEY `authorization_codes_fk_1`;

DROP INDEX `authorization_codes_fk_1` ON `authorization_codes`;

CREATE INDEX `authorization_codes_ibfk_1` ON `authorization_codes` (`client_id`);

ALTER TABLE `authorization_codes` ADD CONSTRAINT `authorization_codes_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `public_keys` DROP FOREIGN KEY `public_keys_fk_1`;

DROP INDEX `public_keys_fk_1` ON `public_keys`;

CREATE INDEX `public_keys_ibfk_1` ON `public_keys` (`client_id`);

ALTER TABLE `public_keys` ADD CONSTRAINT `public_keys_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `refresh_tokens` DROP FOREIGN KEY `refresh_tokens_fk_1`;

DROP INDEX `refresh_tokens_fk_1` ON `refresh_tokens`;

CREATE INDEX `refresh_tokens_ibfk_1` ON `refresh_tokens` (`client_id`);

ALTER TABLE `refresh_tokens` ADD CONSTRAINT `refresh_tokens_ibfk_1`
    FOREIGN KEY (`client_id`)
    REFERENCES `clients` (`client_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `user_attribute_values` DROP FOREIGN KEY `user_attribute_values_fk_1`;

ALTER TABLE `user_attribute_values` DROP FOREIGN KEY `user_attribute_values_fk_2`;

ALTER TABLE `user_attribute_values` DROP FOREIGN KEY `user_attribute_values_fk_3`;

DROP INDEX `user_attribute_values_fk_1` ON `user_attribute_values`;

DROP INDEX `user_attribute_values_fk_2` ON `user_attribute_values`;

DROP INDEX `user_attribute_values_fk_3` ON `user_attribute_values`;

CREATE INDEX `user_attribute_values_ibfk_1` ON `user_attribute_values` (`user_id`);

CREATE INDEX `user_attribute_values_ibfk_2` ON `user_attribute_values` (`attribute_id`, `created_at`);

CREATE INDEX `user_attribute_values_ibfk_3` ON `user_attribute_values` (`attribute_set_id`, `created_at`);

ALTER TABLE `user_attribute_values` ADD CONSTRAINT `user_attribute_values_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

ALTER TABLE `user_attribute_values` ADD CONSTRAINT `user_attribute_values_ibfk_2`
    FOREIGN KEY (`attribute_id`)
    REFERENCES `attributes` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}