<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1481017391.
 * Generated on 2016-12-06 09:43:11 by Marcos
 */
class PropelMigration_1481017391
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `user_attribute_value_sets`;

ALTER TABLE `user_attribute_values` DROP FOREIGN KEY `user_attribute_values_ibfk_3`;

ALTER TABLE `user_attribute_values`

  ADD `date_value` DATETIME AFTER `string_value`;

ALTER TABLE `user_attribute_values` ADD CONSTRAINT `user_attribute_values_ibfk_3`
    FOREIGN KEY (`user_attribute_value_set_id`)
    REFERENCES `attribute_sets` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

CREATE TABLE `attribute_sets`
(
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `attribute_origin_id` INTEGER NOT NULL,
    `created_at` DATETIME NOT NULL,
    `modified_at` DATETIME NOT NULL,
    `deleted_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `attribute_sets_ibfk_1` (`attribute_origin_id`),
    CONSTRAINT `attribute_sets_ibfk_1`
        FOREIGN KEY (`attribute_origin_id`)
        REFERENCES `attribute_origins` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `attribute_sets`;

ALTER TABLE `user_attribute_values` DROP FOREIGN KEY `user_attribute_values_ibfk_3`;

ALTER TABLE `user_attribute_values`

  DROP `date_value`;

ALTER TABLE `user_attribute_values` ADD CONSTRAINT `user_attribute_values_ibfk_3`
    FOREIGN KEY (`user_attribute_value_set_id`)
    REFERENCES `user_attribute_value_sets` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

CREATE TABLE `user_attribute_value_sets`
(
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `attribute_origin_id` INTEGER NOT NULL,
    `created_at` DATETIME NOT NULL,
    `modified_at` DATETIME NOT NULL,
    `deleted_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `user_attribute_value_sets_ibfk_1` (`attribute_origin_id`),
    CONSTRAINT `user_attribute_value_sets_ibfk_1`
        FOREIGN KEY (`attribute_origin_id`)
        REFERENCES `attribute_origins` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}