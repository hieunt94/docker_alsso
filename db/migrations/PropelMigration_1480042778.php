<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1480042778.
 * Generated on 2016-11-25 02:59:38 by ubuntu
 */
class PropelMigration_1480042778
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `ais`

  CHANGE `created` `created_at` DATETIME,

  CHANGE `modified` `modified_at` DATETIME;

ALTER TABLE `chatrooms`

  CHANGE `created` `created_at` DATETIME,

  CHANGE `modified` `modified_at` DATETIME;

ALTER TABLE `friend_requests`

  CHANGE `created` `created_at` DATETIME,

  CHANGE `modified` `modified_at` DATETIME;

ALTER TABLE `friends`

  CHANGE `created` `created_at` DATETIME,

  CHANGE `modified` `modified_at` DATETIME;

ALTER TABLE `messages`

  CHANGE `created` `created_at` DATETIME,

  CHANGE `modified` `modified_at` DATETIME;

ALTER TABLE `tmp_users`

  CHANGE `created` `created_at` DATETIME NOT NULL,

  CHANGE `modified` `modified_at` DATETIME NOT NULL;

DROP INDEX `user_attribute_values_ibfk_2` ON `user_attribute_values`;

ALTER TABLE `user_attribute_values`

  CHANGE `created` `created_at` DATETIME NOT NULL,

  CHANGE `modified` `modified_at` DATETIME NOT NULL;

CREATE INDEX `user_attribute_values_ibfk_2` ON `user_attribute_values` (`attribute_id`, `modified_at`);

DROP INDEX `user_gps_01` ON `user_gps`;

ALTER TABLE `user_gps`

  CHANGE `created` `created_at` DATETIME,

  CHANGE `modified` `modified_at` DATETIME;

CREATE INDEX `user_gps_01` ON `user_gps` (`uid`, `created_at`);

ALTER TABLE `users`

  CHANGE `created` `created_at` DATETIME NOT NULL,

  CHANGE `modified` `modified_at` DATETIME NOT NULL;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `ais`

  CHANGE `created_at` `created` DATETIME,

  CHANGE `modified_at` `modified` DATETIME;

ALTER TABLE `chatrooms`

  CHANGE `created_at` `created` DATETIME,

  CHANGE `modified_at` `modified` DATETIME;

ALTER TABLE `friend_requests`

  CHANGE `created_at` `created` DATETIME,

  CHANGE `modified_at` `modified` DATETIME;

ALTER TABLE `friends`

  CHANGE `created_at` `created` DATETIME,

  CHANGE `modified_at` `modified` DATETIME;

ALTER TABLE `messages`

  CHANGE `created_at` `created` DATETIME,

  CHANGE `modified_at` `modified` DATETIME;

ALTER TABLE `tmp_users`

  CHANGE `created_at` `created` DATETIME NOT NULL,

  CHANGE `modified_at` `modified` DATETIME NOT NULL;

DROP INDEX `user_attribute_values_ibfk_2` ON `user_attribute_values`;

ALTER TABLE `user_attribute_values`

  CHANGE `created_at` `created` DATETIME NOT NULL,

  CHANGE `modified_at` `modified` DATETIME NOT NULL;

CREATE INDEX `user_attribute_values_ibfk_2` ON `user_attribute_values` (`attribute_id`, `modified`);

DROP INDEX `user_gps_01` ON `user_gps`;

ALTER TABLE `user_gps`

  CHANGE `created_at` `created` DATETIME,

  CHANGE `modified_at` `modified` DATETIME;

CREATE INDEX `user_gps_01` ON `user_gps` (`uid`, `created`);

ALTER TABLE `users`

  CHANGE `created_at` `created` DATETIME NOT NULL,

  CHANGE `modified_at` `modified` DATETIME NOT NULL;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}