<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1479977629.
 * Generated on 2016-11-24 17:53:49 by developer
 */
class PropelMigration_1479977629
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `ais`

  CHANGE `deleted` `deleted_at` DATETIME;

ALTER TABLE `chatrooms`

  CHANGE `deleted` `deleted_at` DATETIME;

ALTER TABLE `friend_requests`

  CHANGE `deleted` `deleted_at` DATETIME;

ALTER TABLE `friends`

  CHANGE `deleted` `deleted_at` DATETIME;

ALTER TABLE `messages`

  CHANGE `deleted` `deleted_at` DATETIME;

ALTER TABLE `tmp_users`

  CHANGE `deleted` `deleted_at` DATETIME;

ALTER TABLE `user_attribute_values`

  CHANGE `deleted` `deleted_at` DATETIME;

ALTER TABLE `user_gps`

  CHANGE `deleted` `deleted_at` DATETIME;

ALTER TABLE `users`

  CHANGE `deleted` `deleted_at` DATETIME;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `ais`

  CHANGE `deleted_at` `deleted` DATETIME;

ALTER TABLE `chatrooms`

  CHANGE `deleted_at` `deleted` DATETIME;

ALTER TABLE `friend_requests`

  CHANGE `deleted_at` `deleted` DATETIME;

ALTER TABLE `friends`

  CHANGE `deleted_at` `deleted` DATETIME;

ALTER TABLE `messages`

  CHANGE `deleted_at` `deleted` DATETIME;

ALTER TABLE `tmp_users`

  CHANGE `deleted_at` `deleted` DATETIME;

ALTER TABLE `user_attribute_values`

  CHANGE `deleted_at` `deleted` DATETIME;

ALTER TABLE `user_gps`

  CHANGE `deleted_at` `deleted` DATETIME;

ALTER TABLE `users`

  CHANGE `deleted_at` `deleted` DATETIME;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}